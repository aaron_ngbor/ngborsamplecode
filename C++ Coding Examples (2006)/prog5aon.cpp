// prog5aon.cpp
// Aaron Ngbor
// Computer Science 1428
// Section 2
// Program 5
// Payroll
// Input: This program reads 5 values from an input file. These are the number
   // of hours worked by the employee, second is hourly pay rate, third is the
   // number of dependents claimed by the employee, the fourth and fifth values
   // are the employee's first and last name.
// Output: This program will output the employee name, number of hours worked,
   // the pay rate, the gross pay, the amount of tax withheld, and the net pay,
   // all to an output file.

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

int main()
{
    ifstream fin;                       // The variable for file input
    ofstream fout;                      // The variable for file output
    
    const float OVERTIMEHOUR = 40;      // The highest number of regular work hours
    const float OVERTIMERATE = 1.5;     // Rate of additional overtime pay
    const float HIGHPAY = 480;          // Pay boundary needed for witholding rate
    const float RATE1 = .16;            // High pay high dependency rate
    const float RATE2 = .24;            // High pay mid dependency rate
    const float RATE3 = .32;            // High pay low dependency rate
    const float RATE4 = .10;            // Lower pay high dependency rate
    const float RATE5 = .18;            // Lower pay mid dependency rate
    const float RATE6 = .26;            // Lower pay low dependency rate
    const float LOWDEP = 1;             // Low dependency max
    const float MIDDEP = 3;             // Mid dependency max
    const float HIGHDEP = 4;            // High dependency min
    float workHours;                    // Number of regular hours worked
    float overHours;                    // number of overtime hours worked
    float totalHours;                   // Total number of hours worked
    float payRate;                      // The pay rate
    float numDep;                       // Number of dependents
    string firstName;                   // First name of employee
    string lastName;                    // Last name of employee
    float grossPay;                     // The gross pay
    float taxWithheld;                  // Taxes withheld
    float netPay;                       // The net pay
    
    
    fin.open("prog5.inp");
    fout.open("prog5out.txt");
    
    fin >> workHours;
    fin >> payRate;
    fin >> numDep;
    fin >> firstName;
    fin >> lastName;
    
    if(workHours > OVERTIMEHOUR)
    {
        overHours = workHours - OVERTIMEHOUR;
        workHours = workHours - overHours;
        totalHours = workHours + overHours;
        grossPay = (workHours * payRate) + (overHours *(payRate * OVERTIMERATE));
    }
    else
    {
        grossPay = workHours * payRate;
        totalHours = workHours;
    }
    if(grossPay > HIGHPAY)
    {
        if(numDep >= HIGHDEP)
        {
            taxWithheld = grossPay * RATE1;
        }
        else if(numDep > LOWDEP)
        {
             taxWithheld = grossPay * RATE2;
        }
        else
        {
            taxWithheld = grossPay * RATE3;
        }
    }
    else
    {
        if(numDep >= HIGHDEP)
        {
            taxWithheld = grossPay * RATE4;
        }
        else if(numDep > LOWDEP)
        {
             taxWithheld = grossPay * RATE5;
        }
        else
        {
            taxWithheld = grossPay * RATE6;
        }
    }
    netPay = grossPay - taxWithheld;
    
    fout << "Ngbor Enterprises Payroll\n\n";
    fout << "Employee: ";
    fout << lastName << ", ";
    fout << firstName << "\n";
    fout << "Hours Worked: ";
    fout << totalHours << "\n";
    fout << "Pay Rate: $";
    fout << setprecision(2) << fixed << showpoint;
    fout << payRate << "\n";
    fout << "Gross Pay: $";
    fout << grossPay << "\n";
    fout << "Amount of tax Withheld: $";
    fout << taxWithheld << "\n";
    fout << "Net Pay: $";
    fout << netPay << "\n";
    
    fin.close();
    return 0;
}


