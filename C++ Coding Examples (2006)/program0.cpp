// The Restaurant Bill
// Aaron Ngbor
// Program 0
// This program calculates the total fixed rate meal cost for 2 people


#include <iostream>
using namespace std;

int main ()

{
    float fixedMeal,    // the fixed meal price for 2 people
          tax,          // the tax rate
          tip;          // the tip amount
    fixedMeal = 89;
    tax = 0.0825;
    tip = (fixedMeal + (fixedMeal * tax)) * 0.15;
    cout << "Restaurant Ngbor\n\n";
    cout << "Your Bill Summary\n\n"; 
    cout << "The meal total is $" << fixedMeal << "\n";
    cout << "The tax amount is $" << fixedMeal * tax  << "\n";
    cout << "The tip amount is $" << tip << "\n";
    cout << "The grand total is $" << (fixedMeal + (fixedMeal * tax)) + tip << "\n\n";
    system("pause");
    
}
