#ifndef _FILE_H_
#define _FILE_H_
#include <stdio.h>
/*
 * A circular buffer that stores cstring pointers, with a maximum of ten elements
 */
const int BUFFER_SIZE = 10;
const int LEFT_CLASS = 10 - 1;
const int RIGHT_CLASS = 10 + 1;

typedef struct CircBufferStruct {
	char* buffer[];
	int begin;
	int end;
	int empty;

	int moveLeft(int off){
		return (off + LEFT_CLASS) % BUFFER_SIZE;
	}

	int moveRight(int off){
		return (off + RIGHT_CLASS) % BUFFER_SIZE;
	}

	int bufferIsFull(void){
		//If the offset to the right of the ending offset, modulo the buffer size,
		//is the beginning offset, then the buffer is full
		if(((end + RIGHT_CLASS) % BUFFER_SIZE) == begin){
			return 1;
		}else{
			return 0;
		}
	}
	
	int bufferIsEmpty(void){
		//The buffer is empty iff the beginning and ending offsets are the same		
		if(begin == end){
			return 1;
			empty = 1;
		}else{
			return 0;
		}
	}

	int bufferEnqueue(char* data){
		if(empty){
			buffer[begin] = data;
			empty = 0;
		}else{
			//The buffer will allow overwriting - this moves the beginning forward to accomodate
			if(bufferIsFull()){
				begin = moveRight(begin);
			}
			end = moveRight(end);
			buffer[end] = data;
		}	
	}

	int bufferDequeue(void){
		if(bufferIsEmpty()){
			printf("Nothing to read from - the buffer is empty");
			return 0;
		}else{
			printf("%s",buffer[begin]);
			begin = moveRight(begin);
			return 1;
		}
	}



} CircBuffer;


#endif

		
