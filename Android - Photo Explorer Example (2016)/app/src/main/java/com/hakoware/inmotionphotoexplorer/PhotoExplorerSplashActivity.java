package com.hakoware.inmotionphotoexplorer;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PhotoExplorerSplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_explorer_splash);
        new Handler().postDelayed(new Runnable() {

				/*
				 * Showing splash screen with a timer.
				 */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start the app main activity
                Intent i = new Intent(PhotoExplorerSplashActivity.this, PhotoExplorerMainGridViewActivity.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, 2000);
    }
}
