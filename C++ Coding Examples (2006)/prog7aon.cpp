// prog7aon.cpp
// Aaron Ngbor
// Computer Science 1428
// Section 2
// Program 7
// Test Statistics
// Input: This program reads all of the values from an input file. Each line 
   // contains each student's ID number and test score.
// Output: This program will print statements of the average of the scores, the 
   // number of students who earned As, the number of students who failed the 
   // exam, the ID number and score of the student with the highest score and 
   // the ID number and score of the student with the lowest score for each 
   // section to an output file.


#include<iostream>
#include<iomanip>
#include<fstream>
using namespace std;

int main()
{
    ifstream fin;          // The variable for file input
    ofstream fout;         // The variable for file output
    
    const int A = 90;      // Minimum grade for an A
    const int F = 59;      // Maximum grade for an F
    
    int numClasses;        // Number of classes to be evaluated
    numClasses = 2;        //
    int studID;            // The student's ID
    int testScore;         // The student's test score
    int numA;              // The number of As in the class
    int numF;              // The Number of Fs in the class
    int highScore;         // The high score in the class
    int lowScore;          // The low score in the class
    int highID;            // The student ID of the high score
    int lowID;             // The student ID of the low score
    int total;             // The total of the scores in the class
    int numStudents;       // The number of students in the class
    float averageScore;    // The average score in the class
    
    fin.open("prog7.inp");
    fout.open("prog7out.txt");
    
    fout << "Professor Ngbor's Exam Statistics\n\n";
    int i;                 // The loop index variable
    for(i=1;i<=numClasses;i++)
    {
       numA = 0;
       numF = 0;
       total = 0;
       numStudents = 0;
       fin >> studID;
       fin >> testScore;
       highID = studID;
       lowID = studID;
       highScore = testScore;
       lowScore = testScore;
       while(studID > 0)
       {   
           numStudents++;
           total = (total + testScore);
           if(testScore > highScore)
           {
               highScore = testScore;
               highID = studID;
           }
           else if(testScore < lowScore)
           {
                lowScore = testScore;
                lowID = studID;
           }
           if(testScore >= A)
           {
              numA++;
              fin >> studID;
              fin >> testScore;
           }
           else if(testScore <= F)
           {
              numF++;
              fin >> studID;
              fin >> testScore;
           }
           else
           {
              fin >> studID;
              fin >> testScore;
           }
       }
       averageScore = total / static_cast<double>(numStudents);
       
       
       fout << setprecision(1) << fixed << showpoint;
       fout << "Average Score of class " << i << ": ";
       fout << averageScore << "\n";
       fout << "Number of As in class " << i << ": ";
       fout << numA << "\n";
       fout << "Number of failures in class " << i << ": ";
       fout << numF << "\n";
       fout << "The Student ID with high score in class " << i << ": ";
       fout << highID << " " << highScore << "\n";
       fout << "The Student ID with low score in class " << i << ": ";
       fout << lowID << " " << lowScore << "\n\n";

   }
   
    return 0;
    
}
    
