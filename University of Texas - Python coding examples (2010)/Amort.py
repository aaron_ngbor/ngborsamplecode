# -*- coding: utf-8 -*-
"""
Amort.py
Authors (Noble Team):
	Kiko Ramos
	Aaron Ngbor
	Johanes Johanes
	Oren Fisk
	Addison Denenberg
"""
########
#Import#
########
import sys
import cgi
import re
import decimal

from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import db

from decimal import *

getcontext()
Context(prec=28, rounding=ROUND_HALF_EVEN, Emin=-999999999, Emax=999999999,
        capitals=1, flags=[], traps=[Overflow, DivisionByZero,
        InvalidOperation])
 

class Amort(db.Model):
    """
    Class Amort() is a database model that contains the following values:
      done = db.IntegerProperty()
      ID = db.IntegerProperty()
      pValue = db.FloatProperty()
      fValue = db.FloatProperty()
      pAmount = db.FloatProperty()
      rInterest = db.FloatProperty()
      nPayments = db.IntegerProperty()
      
    These values keep track of valid user input and are used in future calculations
    """
    done = db.IntegerProperty()
    ID = db.IntegerProperty()
    pValue = db.FloatProperty()
    fValue = db.FloatProperty()
    pAmount = db.FloatProperty()
    rInterest = db.FloatProperty()
    nPayments = db.IntegerProperty()

class MainPage(webapp.RequestHandler):

    def get(self):
      """
      Contains html for web page and collects user input. Once all information has been
        gathered and filtered, the table creation will begin.
      """
      #get the Amort result from the data store
      query = db.GqlQuery("SELECT * FROM Amort LIMIT 1")
      amortList = query.fetch(1)
      db.delete(amortList)
      amort = None if len(amortList) == 0 else amortList[0]

      #get the Payments results from the data store for the given Amort result
      paymentList = None

      pValue = ""
      fValue = ""
      pAmount = ""
      rInterest = ""
      nPayments = ""

      #Checks to make sure input values are valid. If not valid, prints "Not Valid." in the input field.
      if (amort != None) :
        pValue = "Not Valid." if ( amort.pValue == -1 ) else str(amort.pValue)
        fValue = "Not Valid." if ( amort.fValue == -1 ) else str(amort.fValue)
        pAmount = "Not Valid." if ( amort.pAmount == -1 ) else str(round(amort.pAmount, 2))
        rInterest = "Not Valid." if ( amort.rInterest == -1 ) else str(amort.rInterest)
        nPayments = "Not Valid." if ( amort.nPayments == -1 ) else str(amort.nPayments)
        done = 0

      self.response.out.write("""
          <html>  

          <head>
              <link type="text/css" rel="stylesheet" href="/stylesheets/main.css" />
          </head>

          <body>
              <h1>Amortization Calculator</h1>
              Welcome to Noble Team's Amort Calculator <br/><br/>
              
              To calculate a correct schedule: <br/><br/>
              &nbsp&nbsp&nbsp&nbsp First fill in two of the following: <br/>
                  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Present Value <br/>
                  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Future Value <br/>
                  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Payment <br/>
                  
              &nbsp&nbsp&nbsp&nbsp <i> *NOTE* If you fill in all three values, ALL THREE will be considered INVALID </i> <br/><br/>
                  
              &nbsp&nbsp&nbsp&nbsp Then fill in the remaining two values: <br/>
                  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Yearly Rate of Interest <br/>
                  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Number of Payments <br/><br/> 
              
              &nbsp&nbsp&nbsp&nbsp Then click submit.<br/><br/>
              
              The payment will be computed and an amortization table will be displayed.<br/><br/>
              All values including the missing value from Amortization Parameters will be shown in the value boxes. <br/><br/>
              The table will be accurate to the penny. <br/><br/>
              
              
              <form action="/" method="post">
                <table class="AmortTable" id="AmortParams">
                    <thead>
                      <h2>Amortization Parameters</h2>
                    </thead>

                    <tbody>
                      <tr>
                          <td>Present Value</td>

                          <td>
                            <input type="text" name="pValue" value=" """ + pValue + """ " />
                          </td>

                          <td>a Non-negative dollar amount (e.g. 75000)</td>
                      </tr>

                      <tr>
                          <td>Future Value</td>

                          <td>
                            <input type="text" name="fValue" value=" """ + fValue + """ "/>
                          </td>

                          <td>a Non-negative dollar amount (e.g. 0)</td>
                      </tr>

                      <tr>
                          <td>Payment</td>

                          <td>
                            <input type="text" name="pAmount" value=" """ + pAmount + """ "/>
                          </td>

                          <td>a Positive non-zero whole number (e.g. 20)</td>
                      </tr>

                      <tr>
                          <td>Yearly Rate of Interest</td>

                          <td>
                            <input type="text" name="rInterest" value=" """ + rInterest + """ "/>
                          </td>

                          <td>a Positive non-zero percent (e.g. 5.24)</td>
                      </tr>

                      <tr>
                          <td>Number of Payments</td>

                          <td>
                            <input type="text" name="nPayments" value=" """ + nPayments + """ "/>
                          </td>

                          <td>a Positive non-zero whole number (e.g. 180)</td>
                      </tr>

                      <tr>
                          <td></td>
                          <td align="center"><input type="submit" value="Submit"/></td>
                          <td></td>
                      </tr>

                    </tbody>

                    <tfoot>
                    </tfoot>
                </table>
              </form>

              <!-- 
              <table class="AmortTable" id="AmortCalc">
                <thead>

                    <h2>amortization calculation</h2>
                </thead>

                <tbody>
                    <tr>
                      <td>Present Value</td>
                      <td></td>
                    </tr>

                    <tr>
                      <td>Future Value</td>
                      <td></td>
                    </tr>

                    <tr>
                      <td>Payment</td>
                      <td></td>
                    </tr>

                    <tr>
                      <td>Monthly Rate of Interest</td>
                      <td></td>
                    </tr>

                    <tr>
                      <td>Number of Payments</td>
                      <td></td>
                    </tr>
                </tbody>

              </table>
              -->

      """)

      self.response.out.write("""
        <table class="AmortTable" id="AmortizationTable">
          <thead>
            <h2>Amortization Table</h2>

            <tr>
                <th>Payment Number</th>
                <th>Balance Before</th>
                <th>Interest</th>
                <th>Total Interest</th>
                <th>Payment</th>
                <th>Total Payment</th>
                <th>Principal</th>
                <th>Total Principal</th>
                <th>Balance After</th>
            </tr>
          </thead>
          <tbody>
      """)

      #Once all inputs have been verified as valid (i.e done = 1), compute and print schedule
      if (amort != None and amort.done != 0) :
        
        #Changing percentage into correct float
        amort.rInterest = amort.rInterest/100

        #initializing all output variables
        paymentCount = 1
        balanceBefore = amort.pValue
        interest = (balanceBefore)*(amort.rInterest/12)
        interest = round(interest, 2)
        totalInterest = interest
        payment = amort.pAmount
        totalPayment = payment
        principal = payment + interest
        totalPrincipal = round(principal, 2)
        balanceAfter = balanceBefore + principal
        totalBalance = round(amort.fValue, 2)

        self.response.out.write("""
          <tr>
            <td align="center">"""+str(1)+ """</td>
            <td align="right">"""+str(round(balanceBefore, 2))+"""</td>
            <td align="right">"""+str(round(interest, 2))+"""</td>
            <td align="right">"""+str(round(totalInterest, 2))+"""</td>
            <td align="right">"""+str(round(payment, 2))+"""</td>
            <td align="right">"""+str(round(totalPayment, 2))+"""</td>
            <td align="right">"""+str(round(principal, 2))+"""</td>
            <td align="right">"""+str(round(totalPrincipal, 2))+"""</td>
            <td align="right">"""+str(round(balanceAfter, 2))+"""</td>
          </tr>
        """)

        #Continue generating table
        for i in range (2, amort.nPayments + 1 ) : 

          balanceBefore = round(balanceAfter, 2) #balanceAfter #
          interest = (balanceBefore)*(amort.rInterest/12)
          interest = round(interest, 2)
          totalInterest += interest
 
          if i == amort.nPayments:
              payment = float(fValue) - (float(balanceBefore) + interest)
          totalPayment += payment
          principal = payment + interest
          totalPrincipal += round(principal, 2)
          balanceAfter = balanceBefore + principal
          
          self.response.out.write("""
            <tr>
              <td align="center">"""+str(i)+ """</td>
              <td align="right">"""+str(round(balanceBefore, 2))+"""</td>
              <td align="right">"""+str(round(interest, 2))+"""</td>
              <td align="right">"""+str(round(totalInterest, 2))+"""</td>
              <td align="right">"""+str(round(payment, 2))+"""</td>
              <td align="right">"""+str(round(totalPayment, 2))+"""</td>
              <td align="right">"""+str(round(principal, 2))+"""</td>
              <td align="right">"""+str(round(totalPrincipal, 2))+"""</td>
              <td align="right">"""+str(round(balanceAfter, 2))+"""</td>
            </tr>
          """)


      self.response.out.write("""

            </tbody>

            <tfoot>
            </tfoot>
          </table>
      </body>
      </html>
      """)

    def post(self):
      """
      Main filter for user inputs. Uses Amort() to hold checked values and redirects to get()
        every time new inputs are given and when calculations of PresentValue, FutureValue, or
        MonthlyPayment has been found.
      """
      maxID = 0
      q = db.GqlQuery("SELECT * FROM Amort")
      for delete in q :
        if (delete.ID > maxID ) :
          maxID = delete.ID
        delete.delete()

      amort = Amort()
      amort.ID = maxID + 1
      amort.done = 0
      
      #Regular expression to filter invalid inputs
      p = re.compile('(\\s*\\-?\\d+(\\.\\d+)?\\s*)|(\\s*\\.\\d+\\s*)')
      r = re.compile('(\\s*\\d+(\\.\\d+)?\\s*)|(\\s*\\.\\d+\\s*)')
      q = re.compile('\\s*\\d+\\s*')

      #Tests and catches exceptions to find invalid inputs
      #Test Present/start Value
      if r.match(self.request.get('pValue'))  :
        try :
          amort.pValue = float(self.request.get('pValue'))
          var = Decimal(str(amort.pValue))
        except Exception:
          amort.pValue = -1.0
          amort.put()
          self.redirect('/')
      else :
        amort.pValue = float(-1.0)
      
      #Test Future Value
      if r.match(self.request.get('fValue')) :
        try :
          amort.fValue = float(self.request.get('fValue'))
          var = Decimal(str(amort.pValue))
        except Exception:
          amort.put()
          self.redirect('/')
      else : 
        amort.fValue = float(-1.0)
      
      #Test Monthly Payment
      if p.match(self.request.get('pAmount')) :
        try :
          amort.pAmount = float(self.request.get('pAmount')) 
          var = Decimal(str(amort.pValue))
        except Exception:
          amort.put()
          self.redirect('/')
       
      else : 
        amort.pAmount = float(-1.0)
      
      #Test Yearly Interest Rate
      if p.match(self.request.get('rInterest')) :
        try :
          amort.rInterest = float(self.request.get('rInterest')) 
          var = Decimal(str(amort.pValue))
        except Exception:
          amort.put()
          self.redirect('/')

      else :
        amort.rInterest = float(-1.0)

      #Test Number of Payments
      if q.match(self.request.get('nPayments')) :
        try :
          amort.nPayments = int(self.request.get('nPayments')) 
          var = Decimal(str(amort.pValue))
        except Exception:
          amort.put()
          self.redirect('/')
      else :
        amort.nPayments = int(-1) 

      
      #Tests multiple invalid fields whether it be invalid or simply blank
      if (amort.rInterest == (-1.0) or amort.rInterest == 0.0) or (amort.nPayments == (-1)) :
        if amort.rInterest == 0.0 :
          amort.rInterest = -1.0
	amort.put()
	self.redirect('/')

      elif ((amort.pValue == (-1.0)) and (amort.fValue == (-1.0))) :
        amort.put() 
        self.redirect('/')

      elif (amort.pValue == -1.0) and (amort.pAmount == (-1.0)) :
        amort.put()
        self.redirect('/')

      elif (amort.fValue == -1.0) and (amort.pAmount == -1.0) :
        amort.put()
        self.redirect('/')

      else :
        #Corrects the decimal place for the yearly interest rate
        newInterest = amort.rInterest/100
        
        #Checks to find the missing value from the input fields
        if (amort.pValue != -1.0) and (amort.fValue != -1.0) and (amort.pAmount != -1.0) :
          amort.pValue = -1.0
          amort.fValue = -1.0
          amort.pAmount = -1.0
          amort.put()
          self.redirect('/')
        
        elif amort.pValue == -1.0 :
          amort.pValue = amort_return_pValue(amort.fValue, amort.pAmount, newInterest, amort.nPayments)
          amort.done = 1
          amort.put()
          self.redirect('/')

        elif amort.fValue == -1.0 :
          amort.fValue = amort_return_fValue(amort.pValue, amort.pAmount, newInterest, amort.nPayments)
          amort.done = 1
          amort.put()
          self.redirect('/')

        else :
          amort.pAmount = amort_return_pAmount(amort.pValue, amort.fValue, newInterest, amort.nPayments)
          amort.done = 1
          amort.put()
          self.redirect('/')


def amort_return_pValue(fValue, pAmount, rInterest, nPayments) :
    """
    INPUT:
      fValue     (float variable) is the given future amount
      pAmount    (float variable) is the given monthly payment
      rInterest  (float variable) is the given yearly interest (this value has already been divided by 100 to be correct) 
      nPayments  (int   variable) is the given number of total payments/months
    OUTPUT:
      pValue     (float variable) is the calculated starting value 
    """
    
    pValue = 0 - ((pAmount * (1-(1/((1+(rInterest/12))**nPayments))))/(rInterest/12) + (fValue/((1 + rInterest/12)**nPayments)) )

    return round(pValue, 2)


def amort_return_fValue(pValue, pAmount, rInterest, nPayments) :
    """
    INPUT:
      pValue     (float variable) is the given starting amount
      pAmount    (float variable) is the given monthly payment
      rInterest  (float variable) is the given yearly interest (this value has already been divided by 100 to be correct) 
      nPayments  (int   variable) is the given number of total payments/months      
    OUTPUT:
      fValue     (float variable) is the calculated future value 
    """
    
    if pAmount > 0 :
      fValue = ((pAmount * (((1 + (rInterest/12))**nPayments)-1))/(rInterest/12) + (pValue * ((1 + rInterest/12)**nPayments)))
      fValue = fValue
    else :
      fValue =  ((pAmount * (((1 + (rInterest/12))**nPayments)-1))/(rInterest/12) + (pValue * ((1 + rInterest/12)**nPayments)))
 
    return round(fValue, 2)


def amort_return_pAmount(pValue, fValue, rInterest, nPayments) :		
    """
    INPUT:
      pValue     (float variable) is the given starting amount
      fValue     (float variable) is the given future amount
      rInterest  (float variable) is the given yearly interest (this value has already been divided by 100 to be correct) 
      nPayments  (int   variable) is the given number of total payments/months
    OUTPUT:
      newP       (float varibale) is the calculated monthly payment
    """
    
    if (pValue > fValue) :
        newP = 0 - (((pValue) / ((1-(1/(((1 + (rInterest/12))**nPayments))))/(rInterest/12))) - ((rInterest/12)*fValue)/(((1+(rInterest/12))**nPayments)-1))
        newP = round(newP - .005, 2)
    else :  
        newP = ((rInterest/12)*fValue)/(((1+(rInterest/12))**nPayments)-1) - ((pValue) / ((1-(1/(((1 + (rInterest/12))**nPayments))))/(rInterest/12)))
        newP = round(newP + .005, 2)
    """
    if newP < 0 :
      round(newP - .005, 2)
    else :
      round(newP + .005, 2)
    """
    return newP 
    
    
application = webapp.WSGIApplication([('/', MainPage)], debug=True)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()

