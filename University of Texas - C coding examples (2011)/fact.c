#include <stdio.h>
#include <stdlib.h>

//Globals
const int numbegin = 48;
const int numend = 57;

int fact_helper(int prev){
	if(prev == 0){
		return 1;}
	return prev * fact_helper(prev-1);
}

int main (int argcount, char* args[]){
	
	//Test for a decimal or any characters
	int is_bad = 0;
	int i = 0;
	while (1){
		if(args[1][i] == 0){
			break;
		}
		if(args[1][i] < numbegin || args[1][i] > numend){
			++is_bad;
			break;
		}
		++i;
	}
	if(is_bad){
		printf("Huh?\n");
	}else{
		int top = atoi(args[1]);
		if(top > 12){
			printf("Overflow\n");
		}else{
			int fact = fact_helper(top);
			printf("%i\n", fact);
		}
	}

	return 0;
}


