
# -------
# imports
# -------
import StringIO
import unittest
import sys
from Primes import primes_read, primes_eval, primality, primes_print

# -----------
# TestPrimes
# -----------

class TestPrimes (unittest.TestCase) :
	
	# ----
    	# read
	# ----
	
	def test_read(self) :
		r = StringIO.StringIO("2\n")		
		n = [0]
		b = primes_read(r, n)
		self.assert_(b == True)
		self.assert_(n[0] == 2)
	

	"""
	 * Testing the evaluation function.
	"""
	def test_eval0(self) :
	
		ret = [0, 0, 0, 0]
		v = primes_eval(7, ret)
		self.assert_(v == 0)
	
	def test_eval1(self) :
	
		ret = [0, 0, 0, 0]
		v = primes_eval(8, ret)
		print ret[0]
		self.assert_(v == 1)
		self.assert_((ret[0] + ret[1] + ret[2] + ret[3]) == 8)
	
	def test_eval2(self) :
	
		ret = [0, 0, 0, 0]
		v = primes_eval(24, ret)
		self.assert_(v == 1)
		self.assert_((ret[0] + ret[1] + ret[2] + ret[3]) == 24)
	
	def test_eval3(self) :
	
		ret = [0, 0, 0, 0]
		v = primes_eval(36, ret)
		self.assert_(v == 1)
		self.assert_((ret[0] + ret[1] + ret[2] + ret[3]) == 36)
	
	def test_eval4(self) :
	
		ret = [0, 0, 0, 0]
		v = primes_eval(46, ret)
		self.assert_(v == 1)
		self.assert_((ret[0] + ret[1] + ret[2] + ret[3]) == 46)
	
	def test_eval5(self) :
	
		ret = [0, 0, 0, 0]
		v = primes_eval(10000000, ret)
		self.assert_(v == 1)
		self.assert_((ret[0] + ret[1] + ret[2] + ret[3]) == 10000000)
	
	def test_eval6(self) :
	
		ret = [0, 0, 0, 0]
		v = primes_eval(95, ret)
		self.assert_(v == 1)
		self.assert_((ret[0] + ret[1] + ret[2] + ret[3]) == 95)
	
	def test_eval7(self) :
	
		ret = [0, 0, 0, 0]
		v = primes_eval(12343, ret)
		self.assert_(v == 1)
		self.assert_((ret[0] + ret[1] + ret[2] + ret[3]) == 12343)
	
	def test_eval8(self) :
	
		ret = [0, 0, 0, 0]
		v = primes_eval(328, ret)
		self.assert_(v == 1)
		self.assert_((ret[0] + ret[1] + ret[2] + ret[3]) == 328)
	
	def test_eval9(self) :
	
		ret = [0, 0, 0, 0]
		v = primes_eval(78956, ret)
		self.assert_(v == 1)
		self.assert_((ret[0] + ret[1] + ret[2] + ret[3]) == 78956)
	

	"""
 	* Testing primality function.
	*
	* First we will test three composite numbers and then
	* we will have it iterate through our entire prime
	* array verifying that the function indeed works.
	"""
	def test_primality0(self)  :
	
		v = primality(24)
		self.assert_(v == 0)
	
	def test_primality1(self) :
	
		v = primality(36)
		self.assert_(v == 0)
	
	def test_primality2(self) :
	
		v = primality(46)
		self.assert_(v == 0)
		
		
	"""
	 * Test the prfunction.
	"""
	def test_print(self)  :
		w = StringIO.StringIO()
		ret = [2, 2, 2, 2]
		primes_print(w, ret, 1)
		self.assert_(w.getvalue() == "2 2 2 2\n")
	
# ----
# main
# ----

print "TestPrimes.py"
unittest.main()
print "Done."



