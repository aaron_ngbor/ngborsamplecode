import java.io.*;
import java.util.*;

public class CovertChannel 
{
    // private fields to contain the fields of the instruction
	private static String execution;
	private static String execution2;
	private static String execution3;
	private static String execution4;
	private static String execution5;
	private static String subject;
	private static String subject2;
	private static String object;
	private static int number;
	
	public CovertChannel(String whichExecution, String whatSubject, String whatObject, int whatNumber)
	{
    		execution = whichExecution;
    		subject = whatSubject;
    		object = whatObject;
    		number = whatNumber;
	}
	
	public String getExecution()
	{
		return execution;
	}
	
	public void setExecution(String otherExecution)
	{
		execution = otherExecution;
	}
	public String getExecution2()
	{
		return execution2;
	}
	
	public void setExecution2(String otherExecution)
	{
		execution2 = otherExecution;
	}
	public String getExecution3()
	{
		return execution3;
	}
	
	public void setExecution3(String otherExecution)
	{
		execution3 = otherExecution;
	}
	public String getExecution4()
	{
		return execution4;
	}
	
	public void setExecution4(String otherExecution)
	{
		execution4 = otherExecution;
	}
	public String getExecution5()
	{
		return execution5;
	}
	
	public void setExecution5(String otherExecution)
	{
		execution5 = otherExecution;
	}
	public String getSubject()
	{
		return subject;
	}
	
	public void setSubject(String newSubject)
	{
		subject = newSubject;
	}
	public String getSubject2()
	{
		return subject2;
	}
	
	public void setSubject2(String newSubject2)
	{
		subject2 = newSubject2;
	}
	public String getObject()
	{
		return object;
	}
	
	public void setObject(String newObject)
	{
		object = newObject;
	}
	
	public int getNumber()
	{
		return number;
	}
	
	public void setNumber(int newNumber)
	{
		number = newNumber;
	}

	
	public static void main(String[] args) throws IOException
	{
			
			//StopWatch s = new StopWatch();
			int bitCounter = 0;
			//s.start();
			try{
				Scanner input = new Scanner(new File("insructionList"));
				int lrr = 0;
				int hrr = 0;
				int lhv = 0;
				int hhv = 0;
				int previous = 0;
        
				while(input.hasNextLine())
				{
					String line = input.nextLine();
					StringTokenizer st = new StringTokenizer(line);
					int c = st.countTokens();
					String currentToken = st.nextToken();
					int counter = 0;

					if((currentToken.equalsIgnoreCase("read") && c == 3) || (currentToken.equalsIgnoreCase("write") && c == 4))
					{
						counter++;
						execution = currentToken;
						currentToken = st.nextToken();
	    		
						if((currentToken.equalsIgnoreCase("hal") || currentToken.equalsIgnoreCase("lyle")) && counter == 1)
						{
							counter++;
							subject = currentToken;
							currentToken = st.nextToken();

							if((currentToken.equalsIgnoreCase("hobj") || currentToken.equalsIgnoreCase("lobj")) && counter == 2)
							{
								counter++;
								object = currentToken;
								if(st.hasMoreTokens())
								{	
									currentToken = st.nextToken();
									
									try{
										int x = Integer.parseInt(currentToken);
										counter = 0;
										number = x;
										System.out.println(subject + " " + execution + "s " + "value " + number + " to " + object);
										System.out.println("The current state is:");
										if((object.equalsIgnoreCase("lobj") && subject.equalsIgnoreCase("lyle")))
										{
											lhv = number;
											System.out.println("   " + object + " has value: " + lhv);
											System.out.println("   HObj has value: " + hhv);
											System.out.println("   Lyle has recently read: " + lrr);
											System.out.println("   Hal has recently read: " + hrr);
											System.out.println();
										}
										else if((object.equalsIgnoreCase("hobj") && subject.equalsIgnoreCase("lyle")))
										{
											hhv = number;
											System.out.println("   LObj has value: " + lhv);
											System.out.println("   " + object + " has value: " + hhv);
											System.out.println("   Lyle has recently read: " + lrr);
											System.out.println("   Hal has recently read: " + hrr);
											System.out.println();
										}
										else if(object.equalsIgnoreCase("hobj") && subject.equalsIgnoreCase("hal"))
										{
											hhv = number;
											System.out.println("   LObj has value: " + lhv);
											System.out.println("   " + object + " has value: " + hhv);
											System.out.println("   Lyle has recently read: " + lrr);
											System.out.println("   Hal has recently read: " + hrr);
											System.out.println();
										}
										else
										{
											System.out.println("   LObj has value: " + lhv);
											System.out.println("   HObj has value: " + hhv);
											System.out.println("   Lyle has recently read: " + lrr);
											System.out.println("   Hal has recently read: " + hrr);
											System.out.println();
										}
		    					
									}catch(NumberFormatException nfe){
										System.out.println("Bad Instruction");
										System.out.println("The current state is:");
										System.out.println("   LObj has value: " + lhv);
										System.out.println("   HObj has value: " + hhv);
										System.out.println("   Lyle has recently read: " + lrr);
										System.out.println("   Hal has recently read: " + hrr);
										System.out.println();
									}
								}
								else
								{
									System.out.println(subject + " " + execution + "s " + object);
									System.out.println("The current state is:");
									if(subject.equalsIgnoreCase("lyle") && object.equalsIgnoreCase("lobj"))
									{
										lrr = lhv;
										System.out.println("   " + object + " has value: " + lhv);
										System.out.println("   HObj has value: " + hhv);
										System.out.println("   Lyle has recently read: " + lrr);
										System.out.println("   Hal has recently read: " + hrr);
										System.out.println();
									}
									else if(subject.equalsIgnoreCase("lyle") && object.equalsIgnoreCase("hobj"))
									{
										lrr = 0;
										System.out.println("   LObj has value: " + lhv);
										System.out.println("   " + object + " has value: " + hhv);
										System.out.println("   Lyle has recently read: " + lrr);
										System.out.println("   Hal has recently read: " + hrr);
										System.out.println();
									}
									else if(subject.equalsIgnoreCase("hal") && object.equalsIgnoreCase("lobj"))
									{
										hrr = lhv;
										System.out.println("   " + object + " has value: " + lhv);
										System.out.println("   HObj has value: " + hhv);
										System.out.println("   Lyle has recently read: " + lrr);
										System.out.println("   Hal has recently read: " + hrr);
										System.out.println();
									}
									else
									{
										hrr = hhv;
										System.out.println("   LObj has value: " + lhv);
										System.out.println("   " + object + " has value: " + hhv);
										System.out.println("   Lyle has recently read: " + lrr);
										System.out.println("   Hal has recently read: " + hrr);
										System.out.println();
									}
								}
							}
						}
					}			
					else 
					{
						System.out.println("Bad Instruction");
						System.out.println("The current state is:");
						System.out.println("   LObj has value: " + lhv);
						System.out.println("   HObj has value: " + hhv);
						System.out.println("   Lyle has recently read: " + lrr);
						System.out.println("   Hal has value: " + hrr);
						System.out.println();
					}
				}
			}catch(FileNotFoundException nfe){

				try{
					Scanner input = new Scanner(new File("inputfilename"));
					FileOutputStream fout;	
					FileOutputStream fout2;	
					fout = new FileOutputStream ("inputfilename.txt");
					fout2 = new FileOutputStream ("log.txt");
					subject = "Hal";
					subject2 = "Lyle";
					execution = "READ";
					execution2 = "WRITE";
					execution3 = "CREATE";
					execution4 = "DESTROY";
					execution5 = "RUN";
					String object2 = "Lobj";
					while(input.hasNextLine())
					{
						String line = input.nextLine();
						while(line.length() == 0)
						{
							new PrintStream(fout).println();
							line = input.nextLine();
							if(!(input.hasNextLine()))
							{
								break;
							}
						}
						if(!(input.hasNextLine()) && line.length() == 0)
						{
							break;
						}
						byte b[] = line.getBytes();
						ByteArrayInputStream input1 = new ByteArrayInputStream(b);
						String binary = Integer.toBinaryString(input1.read());
						String bits = "";
						String covert = "";
						int x = 0;
						while(input1.available() >= 0)
						{
							if(binary.length() < 8)
							{
								int count = 8 - binary.length();
								for(int i = 0; i < count; i++)
								{
									bits = bits + "0";
								}
								bits = bits + binary;
								for(int j = 0; j < bits.length(); j++)
								{
									if(bits.charAt(j) == '0')
									{
										bitCounter++;
										object = "Hobj";
										number = bits.charAt(j);
										new PrintStream(fout2).println(execution5 + " " + subject);
										new PrintStream(fout2).println(execution3 + " " + subject + " " + object);
										new PrintStream(fout2).println(execution3 + " " + subject2 + " " + object2);
										new PrintStream(fout2).println("No op");
										new PrintStream(fout2).println(execution2 + " " + subject2 + " " + object  + " " + "1");
										new PrintStream(fout2).println("No op");
										new PrintStream(fout2).println(execution + " " + subject2 + " " + object);
										new PrintStream(fout2).println("No op");
										new PrintStream(fout2).println(execution4 + " " + subject2 + " " + object);
										object = null;
										new PrintStream(fout2).println(execution5 + " " + subject2);
										if(number == '0')
										{
											covert = covert + "0";
										}
									}
									else
									{
										bitCounter++;
										object = "Hobj";
										number = bits.charAt(j);
										new PrintStream(fout2).println(execution5 + " " + subject);
										new PrintStream(fout2).println(execution3 + " " + subject2 + " " + object2);
										new PrintStream(fout2).println("No op");
										new PrintStream(fout2).println(execution2 + " " + subject2 + " " + object2  + " " + "1");
										new PrintStream(fout2).println("No op");
										new PrintStream(fout2).println(execution + " " + subject2 + " " + object2);
										new PrintStream(fout2).println("No op");
										new PrintStream(fout2).println(execution4 + " " + subject2 + " " + object2);
										object = null;
										new PrintStream(fout2).println(execution5 + " " + subject2);
										if(number == '1')
										{
											covert = covert + "1";
										}
									}
								}
								// when for loop is done, need to convert array elements back to ASCII
								if(covert.equals(bits))
								{
									new PrintStream(fout).print(line.charAt(x));
									x++;
								}
								if(input1.available() > 0)
								{
									binary = Integer.toBinaryString(input1.read());
								}
								else
								{
									break;
								}
								
								covert = "";
								bits = "";
							}
							if(input1.available() == 0)
							{
								int count = 8 - binary.length();
								for(int i = 0; i < count; i++)
								{
									bits = bits + "0";
								}
								bits = bits + binary;
								for(int j = 0; j < bits.length(); j++)
								{
									if(bits.charAt(j) == '0')
									{
										bitCounter++;
										object = "Hobj";
										number = bits.charAt(j);
										new PrintStream(fout2).println(execution5 + " " + subject);
										new PrintStream(fout2).println(execution3 + " " + subject + " " + object);
										new PrintStream(fout2).println(execution3 + " " + subject2 + " " + object2);
										new PrintStream(fout2).println("No op");
										new PrintStream(fout2).println(execution2 + " " + subject2 + " " + object  + " " + "1");
										new PrintStream(fout2).println("No op");
										new PrintStream(fout2).println(execution + " " + subject2 + " " + object);
										new PrintStream(fout2).println("No op");
										new PrintStream(fout2).println(execution4 + " " + subject2 + " " + object);
										object = null;
										new PrintStream(fout2).println(execution5 + " " + subject2);
										if(number == '0')
										{
											covert = covert + "0";
										}
									}
									else
									{
										bitCounter++;
										object = "Hobj";
										number = bits.charAt(j);
										new PrintStream(fout2).println(execution5 + " " + subject);
										new PrintStream(fout2).println(execution3 + " " + subject2 + " " + object2);
										new PrintStream(fout2).println(execution2 + " " + subject2 + " " + object2  + " " + "1");
										new PrintStream(fout2).println(execution + " " + subject2 + " " + object2);
										new PrintStream(fout2).println(execution4 + " " + subject2 + " " + object2);
										object = null;
										new PrintStream(fout2).println(execution5 + " " + subject2);
										if(number == '1')
										{
											covert = covert + "1";
										}
									}
								}
								// when for loop is done, need to convert array elements back to ASCII
								if(covert.equals(bits))
								{
									new PrintStream(fout).print(line.charAt(x));
									x++;
								}
								new PrintStream(fout).println();
								binary = Integer.toBinaryString(input1.read());
								covert = "";
								bits = "";
								break;
							}
						}
					}
					 fout.close();	
					 fout2.close();
				}catch(NumberFormatException nfe1){
					System.out.println("No such file exist!");
				}
			}
			//s.stop();
			//double n = 1000.000;
			//System.out.println("elasped time in seconds: " + (double) s.getElapsedTime()/n);
			//System.out.println("Bandwidth: " + bitCounter/ ((double) s.getElapsedTime()/n));
			//System.out.println("Total bits in file: " + bitCounter);
   } 	
}