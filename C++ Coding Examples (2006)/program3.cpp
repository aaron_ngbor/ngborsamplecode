// program3.cpp
// Aaron Ngbor
// Computer Science 1428
// Section 2
// Program 3
// Widget Orders
// This program reads the data from an input file containing them
// Output: The Program will print the customer name, the number of widgets
           // purchased, the price of a widget, the discount, shipping charge,
           // sales tax, and the total due in an output file

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

int main()
{
    ifstream fin;
    ofstream fout;
    
    const float WIDGETDISC = 0.15;
    const float SHIPPINGCHARGE = 0.02;
    const float TAXRATE = 0.0825;
    string customer;
    string state;
    int numWidget;
    float widgetPrice;
    float totalSalesTax;
    float widgetDiscTotal;
    float shippingPrice;
    float grossTotal;
    float grandTotal;
    
    fin.open("prog3.txt");
    fout.open("prog3out.txt");
    
    fin >> customer;
    fin >> state;
    fin >> numWidget;
    fin >> widgetPrice;
    
    grossTotal = (numWidget * widgetPrice);
    totalSalesTax = (TAXRATE * grossTotal);
    widgetDiscTotal = (WIDGETDISC * grossTotal);
    
    if (numWidget <= 100)
    {
		grandTotal = grossTotal;
	}
	
	else
	{
		grandTotal = (grossTotal - widgetDiscTotal);
	}
	if (grossTotal > 200)
	{
		shippingPrice = 0;		
	}
	else
	{
		shippingPrice = (SHIPPINGCHARGE * numWidget);
	}

	if (state != "tx" && state != "TX")
	{
		grandTotal = grandTotal;
	}
	else
	{
        grandTotal = (grandTotal + shippingPrice + totalSalesTax);
    }
	
    fout << "Aaron Ngbor's World of Widgets\n\n";
	fout << "Customer Name:  ";
	fout << customer << "\n";
	fout << "The Number of Widgets purchased:  ";
	fout << numWidget << "\n";
	fout << "The price of a Widget:  $";
	fout << widgetPrice << "\n";
	fout << "The discount:  $";
	fout << fixed << setprecision(2) << showpoint;
	fout << widgetDiscTotal << "\n";
    fout << "The Shipping Charge:  $";
	fout << shippingPrice << "\n";
	fout << "Sales Tax Amount:  $";
	fout << totalSalesTax << "\n";
	fout << "The Total Due:  $";
	fout << grandTotal << "\n";
	
    return 0;
}
