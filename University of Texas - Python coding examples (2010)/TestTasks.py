"""
Name : Johanes Johanes / Aaron Ngbor
EID  : jj23543	       / aon59
Tasks.py
"""
# -------
# imports
# -------
import StringIO
import unittest
import sys
from Tasks import tasks_read, tasks_eval, tasks_print

# -----------
# TestTasks
# -----------

class TestTasks (unittest.TestCase) :
	
	# ----
    	# read
	# ----
	
	def test_read_1(self) :
		r = StringIO.StringIO("0 0\n")
        	list = []
        	b = tasks_read(r, list)
        	self.assert_(b == False)
        	self.assert_(len(list) == 0)

	def test_read_2(self) :
        r = StringIO.StringIO("5 0\n0 0")
        list = []
        b = tasks_read(r, list)
        self.assert_(b == True)
        self.assert_(len(list) == 5)
        for i in range(len(list)) :
            self.assert_(len(list[i]) == 0)
	

	"""
	  Testing the eval function.
	"""
	def test_eval0(self) :
	
	def test_eval1(self) :
	
	def test_eval2(self) :
	
	def test_eval3(self) :
	
	def test_eval4(self) :
	
	def test_eval5(self) :

	"""
	Comment TBD
	"""

	"""
	 Test Print
	"""
	def test_print(self)  :
	
# ----
# main
# ----

print "TestTasks.py"
unittest.main()
print "Done."
