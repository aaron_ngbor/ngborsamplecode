// ----------------------------
// projects/deque/TestDeque.c++
// Copyright (C) 2010
// Glenn P. Downing
// ----------------------------

/*
To test the program:
    % g++ -ansi -pedantic -lcppunit -ldl -Wall TestDeque.c++ -o TestDeque.app
    % valgrind TestDeque.app >& TestDeque.out
*/

// --------
// includes
// --------

#include <algorithm> // copy, count, fill, reverse
#include <deque>     // deque
#include <memory>    // allocator

#include "cppunit/extensions/HelperMacros.h" // CPPUNIT_TEST, CPPUNIT_TEST_SUITE, CPPUNIT_TEST_SUITE_END
#include "cppunit/TestFixture.h"             // TestFixture
#include "cppunit/TestSuite.h"               // TestSuite
#include "cppunit/TextTestRunner.h"          // TestRunner

#include "Deque.h"

// ---------
// TestDeque
// ---------

template <typename C>
struct TestDeque : CppUnit::TestFixture {
    // ----------------
    // test_constructor
    // ----------------

    void test_constructor () {
        const C x;
        const C y(10);
        const C z(10, 2);
/*
for(unsigned int i = 0; i < z.size(); ++i)

{
 	cout << "TestDeque for loop: " << z[i] << endl;
}
*/
//cout<<"copy constructor"<<endl;
        const C t = z;
	}

    // -------------
    // test_equality
    // -------------

    void test_equality () {
        const C x(10, 4);
        const C y(10, 4);
        assert(x == y);
        assert(!(x != y));
}

  void test_equality_2 () {
        const C i(9, 2);
        const C j(10, 3);
        assert(i != j);
        assert(!(i == j));
}
void test_equality_3 () {
        const C x(10, 4);
        const C y(10, 6);
        assert(x != y);
        assert(!(x == y));
}
    // ---------------
    // test_comparison
    // ---------------

    void test_comparison () {
       const C x(10, 3);
       const C y(20, 4);
       assert(x < y);
       assert(!(x >= y));
       assert(y > x);
       assert(!(y <= x));
}

    void test_comparison_2 () {
       const C x(10, 30);
       const C y(20, 10);
       assert(x > y);
       assert(!(x <= y));
       assert(y < x);
       assert(!(y >= x));
   }





    // ---------------
    // test_assignment
    // ---------------

    void test_assignment () {
              C x(10, 2);
        const C y(20, 3);
        x = y;
/*
	for(unsigned int i = 0; i < x.size(); ++i){
 		cout << "x after copy 1: " << x[i] << endl;
	}
*/
}

    void test_assignment_2 () {
              C x(10, 7);
        const C y(10, 9);
	x = y;
/*
	for(unsigned int i = 0; i < x.size(); ++i){
 		cout << "x after copy 2: " << x[i] << endl;
	}
  */  
    }

    void test_assignment_3 () {
              C x(10, 4);
        const C y(9, 5);

	x = y;
/*
	for(unsigned int i = 0; i < x.size(); ++i){
 		cout << "x after copy 3: " << x[i] << endl;
	}
*/  
  }



    // --------------
    // test_subscript
    // --------------

    void test_subscript () {
              C x(10, 2);
        const C y(10, 2);
        typename C::reference       v = x[0];
        typename C::const_reference w = y[0];
	//cout<< "v is : " << v << " w is : "<< w <<endl; 
        assert(v == w);}

    void test_subscript_2 () {
              C x(10, 2);
        const C y(10, 5);
        typename C::reference       v = x[0];
        typename C::const_reference w = y[0];
	//cout<< "v is : " << v << " w is : "<< w <<endl; 
        assert(v != w);}


    // -------
    // test_at
    // -------

    void test_at () {
              C x(10, 4);
        const C y(10, 4);
	
        typename C::reference       v = x.at(0);
        typename C::const_reference w = y.at(0);
	//cout<< "v is : " << v << " w is : "<< w <<endl;        
	assert(v == w);}

    void test_at_2 () {
              C x(10, 4);
        const C y(10, 10);
	
        typename C::reference       v = x.at(0);
        typename C::const_reference w = y.at(0);
	//cout<< "v is : " << v << " w is : "<< w <<endl;        
	assert(v != w);}


    // ---------
    // test_back
    // ---------

    void test_back () {
              C x(10, 2);
        const C y(10, 2);
        typename C::reference       v = x.back();
        typename C::const_reference w = y.back();
	//cout<< "back of x is : " << v << " back of y is : "<< w <<endl; 
	
        assert(v == w);}

    // ----------
    // test_begin
    // ----------

    void test_begin () {
              C x(10, 2);
        const C y(10, 2);
        typename C::iterator       p = x.begin();
        typename C::const_iterator q = y.begin();
	//cout<< "begin of x is : " << *p << " begin of y is : "<< *q <<endl; 
        assert(*p == *q);}

    // ----------
    // test_clear
    // ----------

    void test_clear () {
        C x(10, 2);
        x.clear();
        assert(x.empty());}

    // ----------
    // test_empty
    // ----------

    void test_empty () {
        const C    x;
        const bool b = x.empty();
        assert(b);}

    // --------
    // test_end
    // --------

    void test_end () {
              C x(10, 2);
        const C y(10, 2);
        typename C::iterator       p = x.end();
        typename C::const_iterator q = y.end();
        assert(*p == *q);}

    // ----------
    // test_erase
    // ----------

    void test_erase () {
        C                    x(10, 2);
        typename C::iterator p = x.erase(x.begin()+3);
        assert(p == x.begin()+3);}

 void test_erase_2 () {
        C   x;
	x.push_back(1);
x.push_back(2);
x.push_back(3);
x.push_back(4);
x.push_back(5);

//cout<<x.size()<<endl;
/*
	for(unsigned int i = 0; i < x.size(); ++i){
		
 		cout << "x after pushing things " << x[i] << endl;

	}
*/
        typename C::iterator p = x.erase(x.begin()+2);
/*
for(unsigned int k = 0; k < x.size(); ++k){
		
 		cout << "x erasing something " << x[k] << endl;

	}
cout<<x.size()<<endl;*/
        assert(p == x.begin()+2);
}

    // ----------
    // test_front
    // ----------

    void test_front () {
              C x(10, 2);
        const C y(10, 2);
        typename C::reference       v = x.front();
        typename C::const_reference w = y.front();
	//cout<< "front of x is : " << v << " front of y is : "<< w <<endl; 
        assert(v == w);}

    // -----------
    // test_insert
    // -----------

    void test_insert () {
        C                    x(10, 2);
        typename C::iterator p = x.insert(x.begin(), 3);
       
	//cout<<"###" << *x.begin()<<endl;
	//cout<<"###" << x.size()<<endl;
/*	
for(unsigned int i = 0; i < x.size(); ++i){
		
 		cout << "x after inserting: " << x[i] << endl;

	}
*/

      assert(p == x.begin());
}


  void test_insert_2 () {
        C                    x(10, 2);
	//x.push_back(19);        

typename C::iterator p = x.insert(x.begin()+5, 3);
       


//	cout<<"###" << *x.begin()<<endl;
//	cout<<"###" << x.size()<<endl;
/*	
for(unsigned int i = 0; i < x.size(); ++i){
		
 		cout << "x after inserting: " << x[i] << endl;

	}
*/

      assert(p == x.begin()+5);
}

    // -------------
    // test_pop_back
    // -------------

    void test_pop_back () {
        C x(10, 2);
        x.pop_back();
	//cout<< "x's size : " << x.size() <<endl;
	}

     void test_pop_back_2 () {
        C x(15, 4);
        x.pop_back();
	//cout<< "x's size : " << x.size() <<endl;
	//cout << "x's back is : " << x.back() << endl;
	}

    




    // --------------
    // test_push_back
    // --------------

    void test_push_back () {
        C x(10, 2);
        x.push_back(3);	
	
	/*
	for(unsigned int i = 0; i < x.size(); ++i){
 		cout << "x after push back 1: " << x[i] << endl;
	}
*/
	//cout<<"capacity is : "<< x.capacity() << endl;
	}

void test_push_back_2 () {
        C x(10, 2);
        x.push_back(3);
	x.push_back(5);	
	x.push_back(6);
	/*
	for(unsigned int i = 0; i < x.size(); ++i){
 		cout << "x after push back 1: " << x[i] << endl;
	}
*/
	//cout<<"capacity is : "<< x.capacity() << endl;
	}

void test_push_back_3 () {
        C x(3, 2);
        x.push_back(3);
	x.push_back(5);	
	x.push_back(6);
	/*
	for(unsigned int i = 0; i < x.size(); ++i){
 		cout << "x after push back 1: " << x[i] << endl;
	}
*/
	//cout<<"capacity is : "<< x.capacity() << endl;
	}

    // -----------
    // test_resize
    // -----------

    void test_resize () {
        C x(10, 2);
/*
for(unsigned int a = 0; a < x.size(); ++a){
 		cout << "x after creation " << x[a] << endl;
	}
        x.resize(20);
for(unsigned int b = 0; b < x.size(); ++b){
 		cout << "x after first resize " << x[b] << endl;
	}
        x.resize(30, 3);
for(unsigned int c = 0; c < x.size(); ++c){
 		cout << "x after second resize " << x[c] << endl;
	}
*/
}

    // ---------
    // test_size
    // ---------

    void test_size () {
        const C x;
        assert(x.size() == 0);}

    // ---------
    // test_swap
    // ---------

    void test_swap () {
        C x(10, 2);
        C y(20, 3);
        x.swap(y);
/*
	for(unsigned int c = 0; c < x.size(); ++c){
 		cout << "x after swap " << x[c] << endl;
	}
*/
}

    // -------------
    // test_iterator
    // -------------

    void test_iterator () {
        C x(10, 2);
        typename C::iterator  b = x.begin();
        assert(b == x.begin());
        typename C::reference v = *b;
        ++b;
        b += 2;
        --b;
        b -= 2;
        typename C::reference w = *b;
        assert(v == w);}

    // -------------------
    // test_const_iterator
    // -------------------

    void test_const_iterator () {
        const C x(10, 2);
        typename C::const_iterator  b = x.begin();
        assert(b == x.begin());
        typename C::const_reference v = *b;
        ++b;
        b += 2;
        --b;
        b -= 2;
        typename C::const_reference w = *b;
        assert(v == w);}

    // ---------------
    // test_algorithms
    // ---------------

    void test_algorithms () {
              C x(10, 2);
        const C y(10, 2);
        assert(std::count(y.begin(), y.end(), 3) == 0);
        std::copy(y.begin(), y.end(), x.begin());
        std::fill(x.begin(), x.end(), 2);
        std::reverse(x.begin(), x.end());}

//test_pop_front
	void test_pop_front () {
        C x(10, 2);
        x.pop_front();

/*
	cout<< "x's size : " << x.size() <<endl;
*/
	}

//test_push_front
   void test_push_front () {
        C x(10, 2);
        x.push_front(3);	
	x.push_front(1000);	
	x.push_front(67);
	}	
	
	/*
	
	for(unsigned int i = 0; i < x.size(); ++i){
 		cout << "x after push front 1: " << x[i] << endl;
	}
	cout<<"capacity is : "<< x.capacity() << endl;
	}
*/

    // -----
    // suite
    // -----

    CPPUNIT_TEST_SUITE(TestDeque);
    CPPUNIT_TEST(test_constructor);
   
    CPPUNIT_TEST(test_equality);
    CPPUNIT_TEST(test_equality_2);
    CPPUNIT_TEST(test_equality_3);
  
    CPPUNIT_TEST(test_comparison);
    CPPUNIT_TEST(test_comparison_2);

  
   CPPUNIT_TEST(test_assignment);
   CPPUNIT_TEST(test_assignment_2);
   CPPUNIT_TEST(test_assignment_3);

    CPPUNIT_TEST(test_subscript);
    CPPUNIT_TEST(test_subscript_2);
    CPPUNIT_TEST(test_at);
    CPPUNIT_TEST(test_at_2);
    CPPUNIT_TEST(test_back);
    CPPUNIT_TEST(test_begin);


    CPPUNIT_TEST(test_clear);

    CPPUNIT_TEST(test_empty);
    // doesn't make sense, end points to a position after the array
    //CPPUNIT_TEST(test_end); 

   CPPUNIT_TEST(test_erase);
	 CPPUNIT_TEST(test_erase_2);
    CPPUNIT_TEST(test_front);

    CPPUNIT_TEST(test_insert);
 CPPUNIT_TEST(test_insert_2);

    CPPUNIT_TEST(test_pop_back);
    CPPUNIT_TEST(test_pop_back_2); 
    CPPUNIT_TEST(test_push_back);
    CPPUNIT_TEST(test_push_back_2);
    CPPUNIT_TEST(test_push_back_3);
    
    
    CPPUNIT_TEST(test_resize);
    
    CPPUNIT_TEST(test_size);
   
    CPPUNIT_TEST(test_swap);

    CPPUNIT_TEST(test_iterator);
    CPPUNIT_TEST(test_const_iterator);

    CPPUNIT_TEST(test_algorithms);
    CPPUNIT_TEST(test_pop_front);
    CPPUNIT_TEST(test_push_front);
  

CPPUNIT_TEST_SUITE_END();};

// ----
// main
// ----

int main () {
    using namespace std;
    ios_base::sync_with_stdio(false);  // turn off synchronization with C I/O
    cout << "TestDeque.c++" << endl;

    CppUnit::TextTestRunner tr;
    tr.addTest(TestDeque< std::deque<int>                       >::suite());
    tr.addTest(TestDeque< std::deque<int, std::allocator<int> > >::suite());
    tr.addTest(TestDeque<      Deque<int>                       >::suite());
    tr.addTest(TestDeque<      Deque<int, std::allocator<int> > >::suite());
    tr.run();

    cout << "Done." << endl;
    return 0;}

