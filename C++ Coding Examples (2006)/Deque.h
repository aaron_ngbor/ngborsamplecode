// ----------------------
// projects/deque/Deque.h
// Copyright (C) 2010
// Glenn P. Downing
// ----------------------

#ifndef Deque_h
#define Deque_h

// --------
// includes
// --------

#include <algorithm> // equal, lexicographical_compare
#include <cassert>   // assert
#include <iterator>  // iterator, bidirectional_iterator_tag
#include <memory>    // allocator
#include <stdexcept> // out_of_range
#include <utility>   // !=, <=, >, >=

// -----
// using
// -----

using std::rel_ops::operator!=;
using std::rel_ops::operator<=;
using std::rel_ops::operator>;
using std::rel_ops::operator>=;

// -------
// destroy
// -------

template <typename A, typename BI>
BI destroy (A& a, BI b, BI e) {
    while (b != e) {
        --e;
        a.destroy(&*e);}
    return b;}

// ------------------
// uninitialized_copy
// ------------------

template <typename A, typename II, typename BI>
BI uninitialized_copy (A& a, II b, II e, BI x) {
    BI p = x;
    try {
        while (b != e) {
            a.construct(&*x, *b);
            ++b;
            ++x;}}
    catch (...) {
        destroy(a, p, x);
        throw;}
    return x;}

// ------------------
// uninitialized_fill
// ------------------

template <typename A, typename BI, typename U>
BI uninitialized_fill (A& a, BI b, BI e, const U& v) {
    BI p = b;
    try {
        while (b != e) {
            a.construct(&*b, v);
            ++b;}}
    catch (...) {
        destroy(a, p, b);
        throw;}
    return e;}

// -----
// Deque
// -----

template < typename T, typename A = std::allocator<T> >
class Deque {
    public:
        // --------
        // typedefs
        // --------

        typedef A allocator_type;
        typedef typename allocator_type::value_type value_type;

        typedef typename allocator_type::size_type size_type;
        typedef typename allocator_type::difference_type difference_type;

        typedef typename allocator_type::pointer pointer;
        typedef typename allocator_type::const_pointer const_pointer;

        typedef typename allocator_type::reference reference;
        typedef typename allocator_type::const_reference const_reference;

        typedef typename allocator_type::pointer iterator;
        typedef typename allocator_type::const_pointer const_iterator;





    public:
        // -----------
        // operator ==
        // -----------

        /**
         * Returns true if the deques are equal.
         */
        friend bool operator == (const Deque& lhs, const Deque& rhs) {
            // <your code>
            // you must use std::equal()
          return (lhs.size() == rhs.size()) && std::equal(lhs.begin(), lhs.end(), rhs.begin());;}

        // ----------
        // operator <
        // ----------

        /**
         * Returns true if first deque is less than the second using a lecographical compare.
         */
        friend bool operator < (const Deque& lhs, const Deque& rhs) {
            // <your code>
            // you must use std::lexicographical_compare()
return std::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());}
            

    private:
        // ----
        // data
        // ----

        allocator_type _a;

        // <your data>
	pointer tb;	// physical beginning
	pointer te;	// physical end
	pointer b;	// logical beginning
	pointer e;	// logical end
	

        

    private:
        // -----
        // valid
        // -----

        bool valid () const {
            // <your code>
	


            return (!tb && !te && !b && !e) || (tb<=b) && (b<=e) && (e<=te);}

   
    

    public:
        // ------------
        // constructors
        // ------------

        /**
         * Constructor for an uninitialized deque
         */
        explicit Deque (const allocator_type& a = allocator_type()) : _a(a) {
            // <your code>
	    tb = te = b = e = 0;
            assert(valid());}

        /**
         * Constructor for a deque with specified size
         */
        explicit Deque (size_type s, const_reference v = value_type(), const allocator_type& a = allocator_type()) : _a(a) {
            // <your code>
		tb = _a.allocate(s);
		b = tb;
		e = tb + s;
		te = tb+s;
		uninitialized_fill(_a, tb, te, v);

/*
		pointer tempbeg=tb;
		while(tempbeg!=te){
			cout<<"explicit deque = "<<*tempbeg<<endl;
			++tempbeg;
		}
*/
            assert(valid());}

        /**
         * Constructor for a deque with specified size and content
         */
        Deque (const Deque& that) : _a(that._a){
            // <your code>
		
	    const size_type s = (that.te - that.tb);
            tb = b = _a.allocate(s);
            te = e = tb + s;
            uninitialized_copy(_a, that.tb, that.te, tb);
	
	   
	   // pointer copybeg = tb;
/*
	while(copybeg!=te){
		cout<<"$$$$"<<*copybeg<<endl;
		++copybeg;
         }
*/
            assert(valid());}

        // ----------
        // destructor
        // ----------

        /**
         * Creating the destructor 
         */
        ~Deque () {			
	if(tb){
  	clear();
		_a.deallocate(tb, capacity());
		tb=b=e=te=0;
	}
            assert(valid());}

        // ----------
        // operator =
        // ----------

        /**
         * The deque assignment operator.
         */
        Deque& operator = (const Deque& that) {
	  if (this == &that)
                return *this;
	  if (that.size() == size())
                std::copy(that.b, that.e, b);
	  else if (that.size() < size()) {
                std::copy(that.b, that.e, b);
                resize(that.size());}
          else if (that.size() <= capacity()) {
                std::copy(that.b, that.b + size(), b);
                e = uninitialized_copy(_a, that.b + size(), that.e, e);}
          else {
                Deque x(that.size());
		x=that;
                swap(x);}
            assert(valid());
            return *this;}

        // -----------
        // operator []
        // -----------

        /**
         * The index operator for deque. Returns the content of a specified index that can be modified.
         */
        reference operator [] (size_type index) {
            // <your code>
            // dummy is just to be able to compile the skeleton, remove it
            
            return  *(b + index);}

        /**
         * The index operator for deque. Returns the content of a specified index that cannot be modified.
         */
        const_reference operator [] (size_type index) const {
            return const_cast<Deque*>(this)->operator[](index);}

        // --
        // at
        // --

        /**
         * Returns the index, but also does out of bounds checking. Can be modified.
         */
     reference at (size_type i) throw (std::out_of_range) {
            if (i >= size())
                throw std::out_of_range("vector::_M_range_check");
            return (*this)[i];}

        /**
         * Returns the index, but also does out of bounds checking. Cannot be modified.
         */
        const_reference at (size_type index) const {
            return const_cast<Deque*>(this)->at(index);}

        // ----
        // back
        // ----

        /**
         * Returns the content of the back index. Can be modified.
         */
        reference back () {
            // <your code>
            // dummy is just to be able to compile the skeleton, remove it
            assert(!empty());
            return *(e - 1);}

        /**
         * Returns the content of the back index. Cannot be modified.
         */
        const_reference back () const {
            return const_cast<Deque*>(this)->back();}

        // -----
        // begin
        // -----

        /**
         * Returns the iterator to the beginning.
         */
        iterator begin () {
            // <your code>
            return b;}

        /**
         * Returns the iterator to the beginning. Cannot be modified.
         */
        const_iterator begin () const {
            // <your code>
            return const_cast<Deque&>(*this).begin();}

        // -----
        // clear
        // -----

        /**
         * Clears out the deque.
         */
        void clear () {
            // <your code>
		resize(0);
            assert(valid());}

        // -----
        // empty
        // -----

        /**
         * Returns true if the deque is empty.
         */
        bool empty () const {
            return !size();}

        // ---
        // end
        // ---

        /**
         * Returns an iterator to the end.
         */
        iterator end () {
            // <your code>
            return e;}

        /**
         * Returns an iterator to the end. Cannot be modified.
         */
        const_iterator end () const {
            // <your code>
            return const_cast<Deque&>(*this).end();}

        // -----
        // erase
        // -----

        /**
         * Erases the contents at a specified index.
         */
        iterator erase (iterator i) {
            // <your code>

        int p = begin() - i;	
        int counter = p*(-1);

	//cout << p <<endl;
 	int ourSize = size();
	pointer tempI = i;

	while(counter < ourSize-1){
	iterator next = tempI+1;
	*tempI = *next;	

	++tempI;
	++counter;
	 }

	resize(size()-1);
/*
         iterator temp = b;
         while(temp!=e){
		cout<<"  "<<*temp<<endl;
		++temp;
         }
*/
/*
	
*/

            assert(valid());
            return i;}

        // -----
        // front
        // -----

        /**
         * Returns the content for the front index.
         */
        reference front () {
            // <your code>
            // dummy is just to be able to compile the skeleton, remove it
            assert(!empty());
            return *b;}

        /**
         * Returns the content for the front index. Cannot be modified.
         */
        const_reference front () const {
            return const_cast<Deque*>(this)->front();}

        // ------
        // insert
        // ------

        /**
         * Inserts a specified element into a specified iterator index.
         */
        iterator insert (iterator i, const_reference x) {

	
	int p = begin() - i;	
        int z = p*(-1);
	resize(size() + 1);
	i = begin()+z;	

	e = std::copy(i, (end()-1), i+1);	 
        *i=x;


	    
            assert(valid());
            return i;}

        // ---
        // pop
        // ---

        /**
         * Removes the last element and adjusts the size.
         */
        void pop_back () {
            // <your code>
	    assert(!empty());
            resize(size() - 1);
            assert(valid());}

        /**
         * Removes the first element and adjusts the size.
         */
        void pop_front () {
            // <your code>
	b = destroy(_a, b, b+1);
	++b;
            assert(valid());}

        // ----
        // push
        // ----

        /**
         * Adds an element to the end of the deque.
         */
        void push_back (const_reference v) {
            // <your code>
          //  resize(size() + 1, v);

	if((size()+1) >= capacity()){
		int mySize = size();
            int newSize = std::max(mySize*2 ,1);

		Deque x(newSize);

                x.e = uninitialized_copy(_a, b, e, x.tb+(newSize-size())/2);

		x.b=x.tb+((newSize-size())/2);
                
		x.e = uninitialized_fill(_a, x.e, x.e+1, v);
		swap(x);
             }
	else{
		e = uninitialized_fill(_a, e, e+1, v);
        }

            assert(valid());}

        /**
         * Adds an element to the front of the deque.
         */
        void push_front (const_reference v) {
            // <your code>
int mySize = size();
	if((size()+1) >= capacity()){
	  int newSize = std::max(mySize*2 ,1);

		Deque x(newSize);
          x.e = uninitialized_copy(_a, b, e, x.tb+(newSize-size())/2);
          x.b=x.tb+((newSize-size())/2);

	  x.b = uninitialized_fill(_a, (x.b-1), x.b, v);
	  x.b= x.b-1;
		swap(x);
	}
	else{
		b = uninitialized_fill(_a, b-1, b, v);
		b=b-1;
        }


            assert(valid());}

        // ------
        // resize
        // ------

        /**
         * Resizes the deque as necessary.
         */
        void resize (size_type s, const_reference v = value_type()) {
            // <your code>
	    if (s == size())
                return;

            if (s < size())
                e = destroy(_a, b + s, e);

            else if (s <= capacity())
                e = uninitialized_fill(_a, e, b + s, v);

            else {
		Deque x(s);
                x.e = uninitialized_copy(_a, b, e, x.tb);
		x.e = uninitialized_fill(_a, x.e, x.e+1, v);
		
		swap(x);

}
            assert(valid());}

        // ----
        // size
        // ----

        /**
         * Gives the total size of the used elements in the deque. Capacity gives the total available space for content to be modified.
         */
        size_type size () const {
            // <your code>
            return e-b;}

	size_type capacity () const {
		return te-tb;}

        // ----
        // swap
        // ----

        /**
         * Swaps the pointers.
         */
        void swap (Deque& that) {
             if (_a == that._a) {
                std::swap(b, that.b);
                std::swap(e, that.e);
		std::swap(tb, that.tb);
                std::swap(te, that.te);}
            else {
                Deque x(*this);
                *this = that;
                that = x;}
            assert(valid());}};

#endif // Deque_h

