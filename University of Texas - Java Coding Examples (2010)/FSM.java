import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Map.Entry;


/*
 * Authors: Aaron Ngbor
 * 			Oren Fisk
 * Date: 10/25/10
 * Description: Builds and traverses finite state machines using a 
 * 				specification .fsm file and a file of strings
 */
public class FSM {
	
	public static void main(String[] args)
	{
		//machine spec
		final String _specFilename = args[0];
		
		//strings
		final String _stringsFilename = args[1];
		
		StateMachine fsm = null;
		
		//build finite state machine from spec
		try {
			System.out.println("Building -----");
			fsm = buildStateMachine(_specFilename);
		} catch (FileNotFoundException e) {
			System.out.println("Specification file "+_specFilename+" could not be found.");
		}
		
		//for each string in strings file, analyze string against state machine
		if(fsm != null)
		{
			try {
				System.out.println("Analyzing -----");
				analyzeStrings(_stringsFilename, _specFilename, fsm);
			} catch (FileNotFoundException e) {
				System.out.println("Strings file "+_stringsFilename+" could not be found.");
			}
		}
		
	}

	private static StateMachine buildStateMachine(String specFilename) throws FileNotFoundException {
		LinkedList<State> states = new LinkedList<State>();
		
		//get file
		File f = new File(specFilename);
		Scanner s = new Scanner(f);
		
		//read each line for state definition
		while(s.hasNext())
		{
			String stateDeclaration = s.nextLine();
			State add = parseStateDeclaration(stateDeclaration, states);
			if(add != null)
				states.add(add);
		}
		
		//pass state definitions to new FSM
		try {
			return new StateMachine(states);
		} catch (NoInitialStateException e) {
			System.out.println("No initial state was specified.");
			return null;
		}
	}
	
	private static State parseStateDeclaration(String stateDeclaration, LinkedList<State> createdStates) {
		System.out.println("Declaration statement: "+stateDeclaration);
		String name = "";
		boolean isInitial = false;
		boolean isTrap = false;
		boolean isAccepting = false;
		
		String[] tokens = stateDeclaration.split("\\s+");
		
		//parse name
		for(int i = 0; i < tokens[0].length(); i++)
		{
			char c = tokens[0].charAt(i);
			if(c == '!')
				isTrap = true;
			else if(c == '@')
				isInitial = true;
			else if(c == '$')
				isAccepting = true;
			else
				name += tokens[0].charAt(i);
		}
		
		//check if this state already exists
		Iterator<State> itr = createdStates.iterator();
		while(itr.hasNext())
		{
			State s = itr.next();
			if(s.getName().equals(name))
			{
				System.out.println("Found existing state.");
				s.addTransitions(tokens);
				return null;
			}
		}
		
		System.out.println("isInitial: "+isInitial+"\n"+
						   "isTrap: "+isTrap+"\n"+
						   "isAccepting: "+isAccepting+"\n"+
						   "name: "+name);
		State result = new State(isInitial, isTrap, isAccepting, name);
		result.addTransitions(tokens);
		
		return result;
	}

	private static void analyzeStrings(String stringsFilename, String specFilename, StateMachine stateMachine) throws FileNotFoundException {
		
		//get file
		File f = new File(stringsFilename);
		Scanner s = new Scanner(f);
		
		String currentString;
		//get all strings
		while(s.hasNext())
		{
			currentString = s.next();
			System.out.println("Current string is: " + currentString);
			boolean accepting = stateMachine.evaluateString(currentString);
			System.out.println("What is the accepting?: " + accepting);
			if(accepting)
			{
				System.out.println(specFilename+" Accepted: "+currentString);
			}
			else
			{
				System.out.println(specFilename+" Rejected: "+currentString);
			}
		}
	}

	static class StateMachine
	{
		//list of states
		private LinkedList<State> _states;
		
		private HashMap<String, Boolean> _acceptingStates;
		
		private State _initial;
		
		public StateMachine(LinkedList<State> states) throws NoInitialStateException
		{
			_initial = null;
			_states = new LinkedList<State>();
			_acceptingStates = new HashMap<String, Boolean>();

			Iterator<State> itr = states.iterator();
			while(itr.hasNext())
			{
				State st = itr.next();
				this.addState(st);
				if(st.isInitial())
					_initial = st;
			}
			
			if(_initial == null)
			{
				throw new NoInitialStateException("No initial class was specified.");
			}
		}

		public void addState(State state)
		{
			_states.add(state);
			if(state.isAccepting())
				_acceptingStates.put(state.getName(), true);
		}
		
		public boolean evaluateString(String string)
		{
			State currentState = _initial;
			char c;
			
			System.out.println("   Evaluating ---");
			System.out.println("      String: "+string);
			System.out.println("        String.length: "+string.length());
			for(int i = 0; i < string.length(); i++)
			{
				System.out.println("Looping: ["+i+"]");
				System.out.println(" Current state: "+currentState.getName());
				if(currentState.isTrap())
				{
					System.out.println("Breaking out of loop.");
					break;
				}
				
				c = string.charAt(i);
				System.out.println("  c: "+c);
				State nextState = currentState.transition(c);
				if(nextState != null)
				{
					currentState = nextState;
				}
				else
				{
					System.out.println("Returning because of null.");
					return false;
				}
			}
			System.out.println(" Current state: "+currentState.getName());
			System.out.println("Returning w/out null.");
			return _acceptingStates.get(currentState.getName());
		}
	}
	
	static class State
	{
		//list of transitions
		private HashMap<Character, State> _transitions;
		private String _name;
		
		private boolean[] _stateTypes;
		
		public State()
		{
			_name = "";
			_stateTypes = new boolean[3];
			_transitions = new HashMap<Character, State>();
		}

		public void addTransitions(String[] transitions) 
		{
			System.out.println("Transitions Length: "+transitions.length);
			//starting at position one since the 0th position of tokens is always the name of the State
			for(int i = 1; i < transitions.length; i++)
			{
				String transition = transitions[i];
				if(transition.equals(" ") || transition.equals(""))
					break;
				System.out.println("Transition: "+transition);
				String[] innerTokens = transition.split(":");
				String nextStateName = innerTokens[0];
				String nextStateTransitions = innerTokens[1];
				System.out.println("Next State: "+nextStateName);
				
				State next = new State(nextStateName);
				String shortcut;
				for(int j = 0; j < nextStateTransitions.length(); j++)
				{
					shortcut = "";
					if(nextStateTransitions.charAt(j) == '|')
					{
						//process shortcuts
						if(nextStateTransitions.charAt(j+1) == 'd')
						{
							shortcut = "0123456789";
						}
						else if(nextStateTransitions.charAt(j+1) == 'n')
						{
							shortcut = "123456789";
						}
						else if(nextStateTransitions.charAt(j+1) == 'a')
						{
							shortcut = "abcdefghijklmnopqrstuvwxyz";
						}
						else if(nextStateTransitions.charAt(j+1) == 's')
						{
							shortcut = "!@#%/&*-+{}.,";
						}
						j += 1;
					}
					
					if(shortcut.length() > 0)
					{
						for(int k = 0; k < shortcut.length(); k++)
						{
							System.out.println("Transition Condition: "+shortcut.charAt(k));
							_transitions.put(shortcut.charAt(k), next);
						}
					}
					else
					{
						System.out.println("Transition Condition: "+nextStateTransitions.charAt(j));
						_transitions.put(nextStateTransitions.charAt(j), next);
					}
				}
			}
			
			System.out.println("Added transitions: ");
			Iterator<Entry<Character, State>> itr =  _transitions.entrySet().iterator();
			while(itr.hasNext())
			{
				Entry<Character, State> entry = itr.next();
				System.out.println("   "+entry.getKey()+"  =>  "+entry.getValue().getName());
			}
		}

		public State(boolean isInitial, boolean isTrap, boolean isAccepting)
		{
			_name = "";
			_stateTypes = new boolean[] {isInitial, isTrap, isAccepting};
			_transitions = new HashMap<Character, State>();
		}
		
		public State(boolean isInitial, boolean isTrap, boolean isAccepting, HashMap<Character, State> transitions, String name)
		{
			_name = name;
			_stateTypes = new boolean[] {isInitial, isTrap, isAccepting};
			_transitions = transitions;
			
			System.out.println("Added transitions: ");
			Iterator<Entry<Character, State>> itr =  _transitions.entrySet().iterator();
			while(itr.hasNext())
			{
				Entry<Character, State> entry = itr.next();
				System.out.println("   "+entry.getKey()+"  =>  "+entry.getValue().getName());
			}
		}
		
		public State(HashMap<Character, State> transitions)
		{
			_name = "";
			_stateTypes = new boolean[3];
			_transitions = transitions;
			
			System.out.println("Added transitions: ");
			Iterator<Entry<Character, State>> itr =  _transitions.entrySet().iterator();
			while(itr.hasNext())
			{
				Entry<Character, State> entry = itr.next();
				System.out.println("   "+entry.getKey()+"  =>  "+entry.getValue().getName());
			}
		}
		
		public State(String name) 
		{
			_name = name;
			_stateTypes = new boolean[3];
			_transitions = new HashMap<Character, State>();
		}

		public State(boolean isInitial, boolean isTrap, boolean isAccepting, String name) 
		{
			_stateTypes = new boolean[] {isInitial, isTrap, isAccepting};
			_name = name;
			_transitions = new HashMap<Character, State>();
		}

		public boolean isInitial()
		{
			return _stateTypes[0];
		}
		
		public boolean isTrap()
		{
			return _stateTypes[1];
		}
		
		public boolean isAccepting()
		{
			return _stateTypes[2];
		}
		
		public State transition(char c) {
			return _transitions.get(c);
		}
		
		public String getName()
		{
			return _name;
		}
	}
	
	static class NoInitialStateException extends Exception
	{
		public NoInitialStateException(String message)
		{
			super(message);
		}
	}
}
