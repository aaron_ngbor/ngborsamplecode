#include "CircBuffer.h"
#include <stdio.h>
	
	int moveLeft(int off){
		return (off + LEFT_CLASS) % BUFFER_SIZE;
	}

	int moveRight(int off){
		return (off + RIGHT_CLASS) % BUFFER_SIZE;
	}

	int bufferIsFull(void){
		//If the offset to the right of the ending offset, modulo the buffer size,
		//is the beginning offset, then the buffer is full
		if((CircBuffer.end + RIGHT_CLASS) % BUFFER_SIZE) == CircBuffer.begin){
			return 1;
		}else{
			return 0;
		}
	}
	
	int bufferIsEmpty(void){
		//The buffer is empty iff the beginning and ending offsets are the same		
		if(CircBuffer.begin == CircBuffer.end){
			return 1;
			empty = 1;
		}else{
			return 0;
		}
	}

	int bufferEnqueue(char* data){
		if(empty){
			CircBuffer.buffer[CircBuffer.begin] = data;
			empty = 0;
		}else{
			//The buffer will allow overwriting - this moves the beginning forward to accomodate
			if(bufferIsFull()){
				CircBuffer.begin = moveRight(CircBuffer.begin);
			}
			CircBuffer.end = moveRight(CircBuffer.end);
			CircBuffer.buffer[CircBuffer.end] = data;
		}	
	}

	int bufferDequeue(void){
		if(bufferIsEmpty()){
			printf("Nothing to read from - the buffer is empty");
			return 0;
		}else{
			printf("%s",CircBuffer.buffer[CircBuffer.begin]);
			CircBuffer.begin = moveRight(CircBuffer.begin);
			return 1;
		}
	}
