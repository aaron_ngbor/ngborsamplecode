// -----------------------------
// projects/collatz/Collatz.java
// Copyright (C) 2010
// Glenn P. Downing
// -----------------------------

import java.io.IOException;
import java.io.Writer;

import java.util.Scanner;

public final class Collatz {
    // ----
    // read
    // ----

    /**
     * reads two ints into a[0] and a[1]
     * @param  r a  java.util.Scanner
     * @param  a an array of int
     * @return true if that succeeds, false otherwise
     */
    public static boolean read (Scanner r, int[] a) {
        if (!r.hasNextInt())
            return false;
        a[0] = r.nextInt();
        a[1] = r.nextInt();
        assert a[0] > 0;
        assert a[1] > 0;
        return true;}

    public static int cacheBuild(long n, int[] store)
    {
        int c = 1;
        while(n > 1)
        {
    			 if(n%2 == 0)
    			{
    				c++;
    				n = n >> 1;				
    			}
    			else
    			{
    				c++;
    				n = (3*n + 1) >> 1;
				c++;
    			}
       }
    	return c;
    }

    public static int helper(long n, int[] store)
    {
        int c = 1;
        while(n > 1)
        {
			if(n < store.length && store[(int) n] != 0)
				return store[(int) n] + c-1;
    			if(n%2 == 0)
    			{
    				c++;
    				n = n >> 1;				
    			}
    			else
    			{
    				c++;
    				n = (3*n + 1) >> 1;
				c++;
    			}
       }
    	return c;
    }

public static int helper2(long n)
    {
        int c = 1;
        while(n > 1)
        {
    			if(n%2 == 0)
    			{
    				c++;
    				n = n >> 1;				
    			}
    			else
    			{
    				c++;
    				n = (3*n + 1) >> 1;
				c++;
    			}
       }
    	return c;
    }
    // ----
    // eval
    // ----

    /**
     * @param  i the beginning of the range, inclusive
     * @param  j the end       of the range, inclusive
     * @return the max cycle length in the range [i, j]
     */
    public static int eval (int i, int j) {
	
        assert i > 0;
        assert j > 0;
if(i > 10000 || j > 10000)
{
	int[] cache = new int[10001];
	for(int alpha = 1; alpha < cache.length; alpha++)
	{
		cache[alpha] = cacheBuild(alpha, cache);
	}
	long count = 0;
        long v = 1;
        if(i <= j)
        {
        while(i <= j)
    		{
    		   count = helper(i, cache);
    		   if(v < count)
    		      v = count;
    		      i++;
    		}
        }
        else
        {
           while(j <= i)
    		{
              
    		   count = helper(j, cache);
    		   if(v < count)
    		      v = count;
    		      j++;
    		}
        }
        assert(v > 0);
        return (int) v;
}
else
{
        long count = 0;
        long v = 1;
        if(i <= j)
        {
        while(i <= j)
    		{
    		   count = helper2(i);
    		   if(v < count)
    		      v = count;
    		      i++;
    		}
        }
        else
        {
           while(j <= i)
    		{
              
    		   count = helper2(j);
    		   if(v < count)
    		      v = count;
    		      j++;
    		}
        }
assert(v > 0);
        return (int) v;
}
        }

    // -----
    // print
    // -----

    /**
     * prints the values of i, j, and v
     * @param  w a java.io.Writer
     * @param  i the beginning of the range, inclusive
     * @param  j the end       of the range, inclusive
     * @param  v the max cycle length
     */
    public static void print (Writer w, int i, int j, int v) throws IOException {
        assert i > 0;
        assert j > 0;
        assert v > 0;
        w.write(i + " " + j + " " + v + "\n");
        w.flush();}}
