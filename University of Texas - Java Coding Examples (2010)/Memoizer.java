import java.util.HashMap;

public class Memoizer 
{
	private HashMap<Object, Object> theMaps = new HashMap<Object, Object>();  
	private Functor temp;
	public Memoizer(Functor f)
	{
		temp = f;
	}
	public Object call(Object x)
	{
		Object fTemp;
		if(theMaps.containsKey(x))
		{
			return theMaps.get(x);
		}
		else
		{
			fTemp = temp.fn(x);
			theMaps.put(x, fTemp);
			return fTemp;
		}
	}
}
