// prog4aon.cpp
// Aaron Ngbor
// Computer Science 1428
// Section 2
// Program 4
// Ink Cartridges
// This program reads the data from an input file
// Output: This program will print out statements of the customer name, 
   // price of the cartridge, quantity ordered, and total cost.
   
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

int main()
{
    ifstream fin;                      // The variable for file input
    ofstream fout;                     // The variable for file output
    
    const float TXTAX = 0.0825;        // Texas Sales Tax
    const float OKTAX = 0.075;         // Oklahoma Sales Tax
    const float DISCOUNTHIGH = 0.5;    // Large dicount
    const float DISCOUNTMIDDLE = 0.4;  // Medium discount
    const float DISCOUNTLOW = 0.3;     // Smallest discount
    const float SHIPPINGCHARGE = 4.0;  // Shipping charge for first cartridge
    const float ADDSHIPCHARGE = 0.25;  // Shipping charge for extra cartridges
    string customer;                   // Customer name
    string state;                      // State abbreviation
    int numInkCartridge;               // Number of ink cartridges purchased
    float inkCartridgePrice;           // Price of each ink cartridge
    float addShip;                     // Additional shipping price
    float totalShipping;               // Total shipping Price
    float discountTotal;               // Total discount 
    float salesTax;                    // The sales tax amount
    float grossTotal;                  // The gross total
    float grandTotal;                  // The grand total
    
    fin.open("prog4.inp");
    fout.open("prog4out.txt");
    
    fin >> customer;
    fin >> state;
    fin >> numInkCartridge;
    fin >> inkCartridgePrice;
    
    grossTotal = numInkCartridge * inkCartridgePrice;
    addShip = numInkCartridge - 1;
    totalShipping = SHIPPINGCHARGE + (addShip * ADDSHIPCHARGE);
    
    
    if(numInkCartridge >= 100)
    {
        discountTotal = (numInkCartridge * inkCartridgePrice) * DISCOUNTHIGH;
        if(state != "tx" && state != "TX" && state != "OK" && state != "ok")
        {
            grandTotal = (totalShipping + grossTotal - discountTotal);
        }
        else if(state == "TX" || state == "tx")
        {
             grandTotal = (totalShipping + grossTotal - discountTotal);
             salesTax = (TXTAX * grandTotal);
             grandTotal = grandTotal + salesTax;
        }
        else
        {
             grandTotal = (totalShipping + grossTotal - discountTotal);
             salesTax = (OKTAX * grandTotal);
             grandTotal = grandTotal + salesTax;
        }
    }
    else if(numInkCartridge >= 50)
    {
        discountTotal = (numInkCartridge * inkCartridgePrice) * DISCOUNTMIDDLE;
        if(state != "tx" && state != "TX" && state != "OK" && state != "ok")
        {
            grandTotal = (totalShipping + grossTotal - discountTotal);
        }
        else if(state == "TX" || state == "tx")
        {
             grandTotal = (totalShipping + grossTotal - discountTotal);
             salesTax = (TXTAX * grandTotal);
             grandTotal = grandTotal + salesTax;
        }
        else
        {
             grandTotal = (totalShipping + grossTotal - discountTotal);
             salesTax = (OKTAX * grandTotal);
             grandTotal = grandTotal + salesTax;
        }
    }
    else if(numInkCartridge >= 20)
    {
         discountTotal = (numInkCartridge * inkCartridgePrice) * DISCOUNTLOW;
        if(state != "tx" && state != "TX" && state != "OK" && state != "ok")
        {
            grandTotal = (totalShipping + grossTotal - discountTotal);
        }
        else if(state == "TX" || state == "tx")
        {
             grandTotal = (totalShipping + grossTotal - discountTotal);
             salesTax = (TXTAX * grandTotal);
             grandTotal = grandTotal + salesTax;
        }
        else
        {
             grandTotal = (totalShipping + grossTotal - discountTotal);
             salesTax = (OKTAX * grandTotal);
             grandTotal = grandTotal + salesTax;
        }
    }
    else
    {
          if(state != "tx" && state != "TX" && state != "OK" && state != "ok")
        {
            grandTotal = (totalShipping + grossTotal);
        }
        else if(state == "TX" || state == "tx")
        {
             grandTotal = (totalShipping + grossTotal);
             salesTax = (TXTAX * grandTotal);
             grandTotal = grandTotal + salesTax;
        }
        else
        {
             grandTotal = (totalShipping + grossTotal);
             salesTax = (OKTAX * grandTotal);
             grandTotal = grandTotal + salesTax;
        }
    }
    fout << "Aaron Ngbor's Ink Cartridges Corp. Order Form\n\n";
    fout << "Customer Name: ";
    fout << customer << "\n";
    fout << "Cartridge Price: $";
    fout << setprecision(2) << fixed << showpoint;
    fout << inkCartridgePrice << "\n";
    fout << "Quantity Ordered: ";
    fout << setprecision(0) << fixed << showpoint;
    fout << numInkCartridge << "\n";
    fout << "Total Cost: $";
    fout << setprecision(2) << fixed << showpoint;
    fout << grandTotal << "\n";
    
    fin.close();
    return 0;
    
}
    
    
    
