#include <stdio.h>

//This is a basic argument printout
int main(int argcount, char* args[]){
	int i = 1;
	while ( i < argcount ) {
		printf("%s\n", args[i]);
		++i;
	}

	return 0;
}
