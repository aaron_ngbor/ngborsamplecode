// prog8aon.cpp
// Aaron Ngbor
// Computer Science 1428
// Section 2
// Program 8
// Cell Phone Bill
// Input: This program reads from an input file. Each line contains a customer
   // account number, the number of calls each customer made, and the number of
   // seconds spent on each call.
// Output: The program will output each account number, total minutes billed,
   // usage cost, tax total, and full total for each custoer, as well as the
   // total of all customers and the average bill for all customers to an output
   // file.



#include<iostream>
#include<iomanip>
#include<fstream>
using namespace std;

ifstream fin;                                  // Variable for file input
ofstream fout;                                 // Variable for file output


void progHeader();


int main()
{
    
    
    const double TAX_RATE = 0.12;             // Tax Rate
    const double LOW_MINUTES = 30;            // Max minutes for low call
    const double SEC_TO_MIN = 60;             // Seconds in a minute
    const double LOW_MINUTES_CHARGE = 0.13;   // Charge for lower minutes
    const double HIGH_MINUTES_CHARGE = 0.04;  // Charge for additional minutes
    int accountNum;                           // The account number
    int numCalls;                             // The number of calls made
    int numSeconds;                           // Number of seconds on each call
    int lastThirty;                           // The additional minutes
    double minutes;                           // The minutes in each call
    double extraTime;                         // Additional seconds to be rounded
    int totalMinutes;                         // Total minutes used per customer
    totalMinutes = 0;                         
    double grossTotal;                        // The gross total
    double taxTotal;                          // The tax total
    double fullTotal;                         // The grand total per customer
    double totalAll;                          // The total of all customers
    int numCustomers;                         // The Number of customers
    double averageBill;                       // The customer average bill
    numCustomers = 0;
    totalAll = 0;
    
    fin.open("prog8inp.txt");
    fout.open("prog8out.txt");
    fin >> accountNum;
    progHeader();
    while(accountNum >= 0)
    {
       numCustomers++;
       totalMinutes = 0;
       fin >> numCalls;
       for(int i = 1; i<=numCalls; i++)
       {
            fin >> numSeconds;
            minutes = (numSeconds / SEC_TO_MIN);
            extraTime = numSeconds % 60;
            if(minutes < 1)
            {
               minutes = 1;
            }
            else if(extraTime > 0)
            {
               minutes = minutes + 1;
            }
            totalMinutes = totalMinutes + minutes;     
               
       }
       if(totalMinutes <= LOW_MINUTES)
       {
          grossTotal = (totalMinutes * LOW_MINUTES_CHARGE);
          taxTotal = grossTotal * TAX_RATE;
          fullTotal = grossTotal + taxTotal;
       }
       else
       {
          grossTotal = LOW_MINUTES * LOW_MINUTES_CHARGE;
          lastThirty = totalMinutes - LOW_MINUTES;
          grossTotal = grossTotal + (lastThirty * HIGH_MINUTES_CHARGE);
          taxTotal = grossTotal * TAX_RATE;
          fullTotal = grossTotal + taxTotal;
       }  
       totalAll = totalAll+fullTotal;
       averageBill = totalAll / numCustomers;
    
       fout << "Account Number: ";
       fout << accountNum << "\n";
       fout << "Total Minutes: ";
       fout << totalMinutes << "\n";
       fout << setprecision(2) << fixed << showpoint;
       fout << "Usage Cost: ";
       fout << grossTotal << "\n";
       fout << "Total Tax: ";
       fout << taxTotal << "\n";
       fout << "Total Due: ";
       fout << fullTotal << "\n\n";
       fin >> accountNum;
       }
    fout << "Total of All: ";
    fout << totalAll << "\n";
    fout << "Average Bill: ";
    fout << averageBill << "\n\n";
    
    fin.close();
    return 0; 
}

// Definition of function progHeader
// This function displays the heading to the output file
void progHeader()
{
     fout << "Ngbor Mobile Billing Summaries\n\n";
}

   
