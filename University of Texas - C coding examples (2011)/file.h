#ifndef FILE_H_
#define FILE_H_

int bufferIsFull(void);
int bufferIsEmpty(void);
int bufferEnqueue(char *data);
int bufferDequeue(void);
void bufferPrint(void);



#endif

