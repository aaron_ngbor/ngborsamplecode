package com.hakoware.restfulweathersample;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.location.Location;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WeatherSampleMainActivity extends AppCompatActivity implements View.OnClickListener,
        LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 0;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    // class variables
    private long mLastTime;
    private String latitudeText = "";
    private String longitudeText = "";

    // Android widgets
    private EditText latitudeInput;
    private EditText longitudeInput;
    private Button btnGetCoodinateData;
    private Button btnGetMyLocationData;
    private ProgressBar progressBar;
    private GoogleApiClient mGoogleApiClient;
    private Location location = null;

    // string constants
    private final String API_URL = "http://api.openweathermap.org/data/2.5/weather?";
    private final String OPEN_MAP_API_KEY = "7cee2c704077ec90541dc7a890a34e7d";
    private final long UPDATE_INTERVAL = 5000;
    private final long FASTEST_INTERVAL = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_sample_main);

        // Variable definitions
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mLastTime = System.currentTimeMillis();
        latitudeInput = (EditText) findViewById(R.id.inputLatitude1);
        longitudeInput = (EditText) findViewById(R.id.inputLongitude1);
        btnGetCoodinateData = (Button) findViewById(R.id.btnAccessApiSearch);
        btnGetMyLocationData = (Button) findViewById(R.id.btnAccessApiCurrentLoc);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        btnGetCoodinateData.setOnClickListener(this);
        btnGetMyLocationData.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(servicesAvailable()) {
            mGoogleApiClient.connect();
        }
    }

    private boolean servicesAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        return (ConnectionResult.SUCCESS == resultCode);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onConnected(Bundle bundle) {
        // get current location
        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        btnGetMyLocationData.setEnabled(true);

        // request update every 1-5 seconds with high accuracy
        LocationRequest locationRequest = LocationRequest.create();
        int permissionCheckFine = ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionCheckCoarse = ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION);

        // test on a real device
        if (permissionCheckFine != PackageManager.PERMISSION_GRANTED || permissionCheckCoarse != PackageManager.PERMISSION_GRANTED) {
            btnGetMyLocationData.setEnabled(false);
            Toast.makeText(getApplicationContext(), "Cannot retrieve current location weather info" +
                    " without location permissions. Will use last known location.", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
        }
        permissionCheckFine = ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        permissionCheckCoarse = ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION);
        if(permissionCheckFine == PackageManager.PERMISSION_GRANTED || permissionCheckCoarse == PackageManager.PERMISSION_GRANTED) {
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            locationRequest.setInterval(UPDATE_INTERVAL);
            locationRequest.setFastestInterval(FASTEST_INTERVAL);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
        }


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btnAccessApiSearch :
                latitudeText = latitudeInput.getText().toString().trim();
                longitudeText = longitudeInput.getText().toString().trim();
                if(latitudeText.equals("")) {
                    Toast.makeText(v.getContext(), "Please enter a latitude.", Toast.LENGTH_SHORT).show();
                }
                else if(longitudeText.equals("")) {
                    Toast.makeText(v.getContext(), "Please enter a longitude.", Toast.LENGTH_SHORT).show();
                }
                else {
                    makeStringForAPI(latitudeText, longitudeText);
                }
                break;
            case R.id.btnAccessApiCurrentLoc :
                makeStringForAPI(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
                break;
        }

    }

    /**
     *
     * @param latCoordinate
     * @param longCoordinate
     */
    public void makeStringForAPI(String latCoordinate, String longCoordinate) {
        String dataUrl = API_URL + "lat=" + latCoordinate + "&lon=" + longCoordinate + "&units=imperial&appid=" + OPEN_MAP_API_KEY;
        new CallAPI().execute(dataUrl);
    }

    /**
     *
     */
    private class CallAPI extends AsyncTask<String, String, String> {

        Exception mException = null;

        /**
         * Method to make the URL connection,get the resulting JSON, and disconnect the URL connection
         *
         * @param params
         * @return
         */
        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            try {
                URL url = new URL(urlString);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(String response) {
            if (response == null) {
                return;
            } else {
                Intent weatherResults = new Intent(getApplicationContext(), WeatherSampleResultsActivity.class);
                progressBar.setVisibility(View.GONE);
                weatherResults.putExtra("result_json_set", response);
                startActivity(weatherResults);
            }
        }
    }
}
