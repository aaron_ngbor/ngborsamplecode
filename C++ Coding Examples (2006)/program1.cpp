// program1.cpp
// Aaron Ngbor
// Computer Science 1428
// Section 2
// Program 1
// Movie Theater Profit
// Input:  The user will be prompted to enter the number of adult tickets sold and
//     the number of children's tickets sold.
// Output and purpose: The program will print the number of adult tickets sold, the number of child tickets sold, 
//     the gross receipts from ticket sales, the amount paid to the distributor, 
//     the profit from ticket sales, the snack bar profit, and the total profit.

#include <iostream>
#include <iomanip>
using namespace std;


int main()
{
    cout << setprecision(2) << fixed << showpoint;
    
    const double ADULT_TICKET = 8.75;   // Price of Adult ticket
    const double CHILD_TICKET = 5.00;   // Price of Child Ticket
    const double distProfitPercent = 0.75;    // Percent Profit Given to Movie Distributor
    int numAdult;    // Number of Adult Tickets
    int numChild;    // Number of Child Tickets
    double snackAdult;    // Adult Snack Price
    double snackChild;    // Child Snack Price
    double sbProfit;   // Profit made by snack bar
    double grossProfit;    // Gross Profit of Theater
    double distProfit;    // Profit of movie distributor
    double boProfit;    // Ticket Profit of theater
    double totalProfit;    // Total Profit of theater

    
    cout << "Aaron's Theaters Money Summary\n\n";
    cout << "Please enter the number of adult tickets sold\n";
    cin >> numAdult;
    cout << "\n";
    cout << "Please enter the number of child tickets sold\n";
    cin >> numChild;
    cout << "\n";
    cout << "Total Adult Tickets: " << numAdult << "\n";
    cout << "Total Child Tickets: " << numChild << "\n";
    grossProfit = numAdult * ADULT_TICKET + numChild * CHILD_TICKET;
    cout << "Gross Box Office Receipt: $" << grossProfit << "\n";
    distProfit = (grossProfit * distProfitPercent);
    cout << "Total Paid to Movie Distributor: $" << distProfit << "\n";
    boProfit = grossProfit - distProfit;
    cout << "Net Box Office Receipt: $" << boProfit << "\n";
    snackAdult = numAdult * 2.25;
    snackChild = numChild * 1.75;
    sbProfit = snackAdult + snackChild;
    cout << "Total Snack Bar Profit: $" << sbProfit << "\n";
    totalProfit = sbProfit + boProfit;
    cout << "Total Profit: $" << totalProfit << "\n\n";
    system("PAUSE");

    
    
}
