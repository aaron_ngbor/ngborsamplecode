package com.hakoware.inmotionphotoexplorer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PhotoExplorerMainGridViewActivity extends Activity {
    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapter mGridAdapter;
    private ArrayList<GridItem> mGridData;
    private String IMG_URL = "http://jsonplaceholder.typicode.com/photos";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_explorer_main_grid_view);

        mGridView = (GridView) findViewById(R.id.gridView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        //Initialize with empty data
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapter(this, R.layout.photo_item, mGridData);
        mGridView.setAdapter(mGridAdapter);

        // This makes the url calll to get the JSON for the gridView
        new CallURLForJSON().execute(IMG_URL);

    }



    /**
     *
     */
    private class CallURLForJSON extends AsyncTask<String, String, String> {

        Exception mException = null;

        /**
         * Method to make the URL connection,get the resulting JSON, and disconnect the URL connection
         *
         * @param params
         * @return
         */
        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            try {
                URL url = new URL(urlString);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    // parseResult to populate the gridView with the JSON data
                    parseResult(stringBuilder.toString());
                    return stringBuilder.toString();
                }
                finally {
                    urlConnection.disconnect();
                }
            }
            catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(String response) {
            // Only try to update the UI if the data was retrieved
            if (response != null) {
                mGridAdapter.setGridData(mGridData);
            }
            else {
                Toast.makeText(PhotoExplorerMainGridViewActivity.this, "There was an error when retrieving the data.", Toast.LENGTH_SHORT).show();
            }
            mProgressBar.setVisibility(View.GONE);
        }

        /**
         * This method parses the results, gets the title for each image, and the image itself.
         * The method assumes the JSON is formatted with a "title" string and "url" string in a JSON array
         * @param result
         */
        private void parseResult(String result) {
            try {
                JSONArray response = new JSONArray(result);
                GridItem item;
                for (int i = 0; i < response.length(); i++) {
                    JSONObject post = response.optJSONObject(i);
                    String title = post.optString("title");
                    item = new GridItem();
                    item.setTitle(title);
                    item.setImage(post.getString("url"));
                    mGridData.add(item);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
