// prog10aon.cpp
// Aaron Ngbor
// Computer Science 1428
// Section 2
// Program 10
// Test scores
// Input: This program reads the needed data from an input file into arrays. 
   // Each line contains the student and thier grade.
// Output: The program will output the class average, each student name, their 
   // grade, the letter grade they obtained, and the names of the A students to
   // an output file.


#include<iostream>
#include<iomanip>
#include<fstream>
#include<string>
using namespace std;


int main()
{
    ifstream fin;                             // Variable for file input
    ofstream fout;                            // Variable for file output
    
    const int NUM_STUDENTS = 33;              // Number of students in class
    int grade[NUM_STUDENTS];                  // Array to hold student grade
    string name[NUM_STUDENTS];                // Array to hold student name
    string letterGrade[NUM_STUDENTS];         // Array to hold the letter grade
    int sum;                                  // The sum of all grades
    sum = 0;
    double average;                           // The averages of all grades
    average = 0;
    int gradeRange;                           // The range for grades
    gradeRange = 10;
 
    fin.open("prog10inp.txt");
    fout.open("prog10out.txt");
    
    
    for (int i=1; i<NUM_STUDENTS; i++)
    {
        fin >> name[i];
        fin >> grade[i];
        sum = sum+grade[i];         
    }
    average = (sum / NUM_STUDENTS);
    fout << "Professor Ngbor's Test Scores\n\n";
    fout << setprecision(1) << fixed << showpoint;
    fout << "Class Average: " << average << "\n\n";
    for(int i=1; i<NUM_STUDENTS; i++)
    {
        if(grade[i] > (average+gradeRange))
        {
            letterGrade[i] = "A";
        }
        else if(grade[i] <= (average+gradeRange) && grade[i] >= (average-gradeRange))
        {
            letterGrade[i] = "C";
        }
        else
        {
            letterGrade[i] = "F";
        }
    }
    for(int i=1; i<NUM_STUDENTS; i++)
    {
        fout << "Name: " << name[i] << "     Grade: " << grade[i] << "     Letter Grade: " << letterGrade[i] << "\n\n";
    }
    fout << "The A Students\n\n";
    for(int i=1; i<NUM_STUDENTS; i++)
    {
        if(letterGrade[i] == "A")
        {
            fout << name[i] << "\n";
        }  
    }
    fout << "\n";
    

    return 0;
}   
    
