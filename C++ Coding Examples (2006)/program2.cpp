// program2.cpp
// Aaron Ngbor
// Computer Science 1428
// Section 2
// Program 2
// Fuel Efficiency and Car Expenses
// This program actually reads the file containing numeric values from the computer.
    // It will take the miles driven, number of gallons used, total miles driven,
    // and the price for a gallon of gas.
// Output: The program will print the three calculated values to an output file.

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

int main()
{
    ifstream fin;  
    ofstream fout;
    
    int testPeriodMiles;  // Number of miles driven during test period
    double gallons;    // Number of gallons used during test period
    int totalYearMiles;  // Total miles driven in a year
    double gasPrice;    // Price of a gallon of gas
    
    fin.open("prog2.txt");
    fout.open("prog2out.txt");
    
    fin >> testPeriodMiles;
    fin >> gallons;
    double milesPerGallon;    // Miles per gallon of the car    
    milesPerGallon = testPeriodMiles / gallons;
    fout << setprecision(1) << fixed << showpoint;
    fout << "Test Drive Fuel Efficiency and Cost Summary\n\n";
    fout << "The miles per gallon obtained is: ";
    fout << milesPerGallon << "\n";
    fin >> totalYearMiles;
    fin >> gasPrice;
    double yearFuelCost;    // The fuel cost of a given year of driving
    yearFuelCost = (totalYearMiles / milesPerGallon) * gasPrice;
    fout << setprecision(2) << fixed << showpoint;
    fout << "The Yearly Cost of Fuel is: $";
    fout << yearFuelCost << "\n";
    double pricePerMile;    // The price for each mile driven
    pricePerMile = gasPrice / milesPerGallon;
    fout << "The Fuel Cost for each mile driven is: $";
    fout << pricePerMile << "\n\n";
    
    system("PAUSE");
    return 0;
    
    
  
}
