#include <assert.h>
#include <stdio.h>
#include <math.h>
#include "point.h"

/*
 * Update *p by increasing p->x by x and 
 * p->y by y
 */
void point_translate(Point *p, double x, double y){
	point_set(p, x+point_getX(p), y+point_getY(p));
}

/*
 * Return the distance from p1 to p2
 */
double point_distance(const Point *p1, const Point *p2){
	double xdiff = point_getX(p1) - point_getX(p2);
	double ydiff = point_getY(p1) - point_getY(p2);
	double xsquare = xdiff * xdiff;
	double ysquare = ydiff * ydiff;
	double val = sqrt(xsquare + ysquare);
	
	return val;
}
