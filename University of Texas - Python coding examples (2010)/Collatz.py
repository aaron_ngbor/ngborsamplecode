#!/usr/bin/env python

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2010
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------

def collatz_read (r, a) :
    """
    reads two ints into a[0] and a[1]
    r is a  reader
    a is an array on int
    return true if that succeeds, false otherwise
    """
    s = r.readline()
    if s == "" :
        return False
    l = s.split()
    a[0] = int(l[0])
    a[1] = int(l[1])
    assert a[0] > 0
    assert a[1] > 0
    return True

# ------------
# collatz_eval
# ------------

def cacheBuild(n, store) :
	"""
    	builds a cache of 10000 cycle lengths
    	n is the current value to be calculated
    	store is an array of int
    	return c, the cycle length of n
    	"""
	c = 1
	while n > 1 :
		if n%2 == 0 :
			c += 1
			n = n >> 1
		else :
			c += 1
			n = (3*n + 1) >> 1
			c += 1
	return c

def helper(n, store) :
	"""
    	computes the cycle length for the given value
    	n is the current value to be calculated
    	store is an array of int that will be checked if n is already inside
    	return c, the cycle length of n
    	"""	
	c = 1
   	while n > 1 :
		if n < len(store) and store[n] != 0 :
			return store[n] + c-1
	   	elif n%2 == 0 :
		   	c += 1
		   	n = n >> 1
	   	else :
	   	   	c += 1
		   	n = (3*n + 1) >> 1
		   	c += 1
   	return c
	
def helper2(n) :
	"""
	computes the cycle length for the given value
    	n is the current value to be calculated
    	store is an array of int that will be checked if n is already inside
    	return c, the cycle length of n
	"""
	c = 1
	while n > 1 :
		if n%2 == 0 :
			c += 1
			n = n >> 1
		else :
			c += 1
			n = (3*n + 1) >> 1
			c += 1
	return c
		
def collatz_eval (i, j) :
    	"""
    	i is the beginning of the range, inclusive
    	j is the end       of the range, inclusive
    	return the max cycle length in the range [i, j]
    	"""
    	assert i > 0
    	assert j > 0
    	if i > 10000 or j > 10000 :
		cache = [0] * 10001
		temp = len(cache)
		for alpha in range (1, temp) :
		   	cache[alpha] = cacheBuild(alpha, cache)
    	    	count = 0
    	    	v = 1
    	    	if i <= j :
	      		while i <= j :
		    		count = helper(i, cache)
		    		if v < count :
			    		v = count
	                    	i += 1
    	    	else :
              		while j <= i :
	         		count = helper(j, cache)
	         		if v < count :
	            			v = count
	            		j += 1
            	assert v > 0
            	return v
    	else :
	  	count = 0
    	  	v = 1
    	  	if i <= j :
	  		while i <= j :
		    		count = helper2(i)
		    		if v < count :
			    		v = count
	           		i = i + 1
    	  	else :
             		while j <= i :
	        		count = helper2(j)
	        		if v < count :
	              			v = count
	        		j += 1
	assert v > 0
        return v  

# -------------
# collatz_print
# -------------

def collatz_print (w, i, j, v) :
    """
    prints the values of i, j, and v
    i is the beginning of the range, inclusive
    j is the end       of the range, inclusive
    v is the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")
