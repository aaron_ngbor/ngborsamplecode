package com.aaronngbor.billcalculatorplus;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.io.*;
import java.util.Scanner;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

public class BillCalculatorPlus extends Activity implements AdapterView.OnItemSelectedListener, RadioGroup.OnCheckedChangeListener, OnClickListener {
	
	// for the different variables needed for computation
	TextView selection;
	TextView tip;
	TextView numPeopleName;
	TextView numberPeopleOrItems;
	TextView costItems;
	TextView initialTotal;
	TextView grandTotal;
	TextView gratuityString;
	TextView tipAmountString;
	TextView otherEntry;
	TextView totalBeforeTipString;
	TextView evenSplitString;
	TextView evenSplit;
	
	// the add and reset buttons
	ImageButton add;
	ImageButton reset;
	
	// the radio buttons for the mode and the different settings for the tip value
	RadioGroup mode;
	RadioGroup tipValues;
	
	// variables to hold the computed values
	double total = 0.00;
	double finalTotal = 0.00;
	double tipValue = 0.00;
	double salesTax = 0.00;
	
	// boolean for whether at a restaurant or standard shopping
	boolean restaurantExpense = true;
	
	DecimalFormat df = new DecimalFormat("#.##");
	
	
	// map for the state names with states as keys, sales tax as values
	Map<String, Double> stateTaxes = new HashMap<String, Double>();
	
	String[] states ={"Alabama", "Alaska", "Arizona", "Arkansas", "California",
					"Colorado", "Connecticut", "Delaware", "Florida", "Georgia",
					"Hawaii", "Idaho", "Illinois", "Indiana", "Iowa",
					"Kansas", "Kentucky", "Louisiana", "Maine", "Maryland",
					"Massachusetts", "Michigan", "Minnesota", "Mississippi", 
					"Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire",
					"New Jersey", "New Mexico", "New York", "North Carolina",
					"North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania",
					"Rhode Island", "South Carolina", "South Dakota", "Tennessee",
					"Texas", "Utah", "Vermont", "Virginia", "Washington",
					"West Virginia", "Wisconsin", "Wyoming"};
	Double[] taxValues = {10.0, 7.0, 10.6, 9.25, 9.75, 8.0, 6.35, 0.0, 7.5, 8.0,
			4.712, 6.0, 11.5, 9.0, 7.0, 8.65, 6.0, 10.0, 5.0, 6.0, 6.25, 6.0, 7.875, 
			9.0, 9.241, 3.0, 7.0, 8.1, 0.0, 7.0, 8.5625, 8.875, 7.25, 7.0, 7.75, 8.5,
			5.0, 8.0, 7.0, 9.0, 6.0, 9.75, 8.25, 8.35, 7.0, 5.0, 9.5, 6.0, 5.6, 7.0};
	
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.main);
		setTaxMap();
		selection=(TextView)findViewById(R.id.selection);
		mode = (RadioGroup) findViewById(R.id.billMode);
		tipValues=(RadioGroup) findViewById(R.id.tipRadioButtons);
		gratuityString = (TextView) findViewById(R.id.gratuityString);
		totalBeforeTipString = (TextView) findViewById(R.id.totalBeforeTipString);
		tipAmountString = (TextView) findViewById(R.id.tipAmountString);
		tip = (TextView) findViewById(R.id.tipTotal);
		numPeopleName = (TextView) findViewById(R.id.numPeopleName);
		numberPeopleOrItems = (TextView) findViewById(R.id.numPeople);
		costItems = (TextView) findViewById(R.id.itemCost);
		initialTotal = (TextView) findViewById(R.id.total_before_tip);
		grandTotal = (TextView) findViewById(R.id.grandTotal);
		evenSplit = (TextView) findViewById(R.id.evenSplit);
		evenSplitString = (TextView) findViewById(R.id.evenSplitString);
		otherEntry = (TextView) findViewById(R.id.otherEntry);
		add = (ImageButton) findViewById(R.id.addButton);
		reset = (ImageButton) findViewById(R.id.resetButton);
		Spinner spin=(Spinner)findViewById(R.id.spinner);		
		numPeopleName.setText("No. of People");
		mode.check(R.id.restaurant_mode);
		mode.setOnCheckedChangeListener(this);
		add.setOnClickListener(this);
		reset.setOnClickListener(this);
		spin.setOnItemSelectedListener(this);	
		ArrayAdapter<String> aa=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, states);
		aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin.setAdapter(aa);
		tipValues.setOnCheckedChangeListener(this);
	}
	
	// putting the key value pairs into the map
	public void setTaxMap() 
	{
		for(int i = 0; i < 50; i++)
		{
			stateTaxes.put(states[i], taxValues[i]);
		}
		
	}

	// for the menu with an about and an exit
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
	      super.onCreateOptionsMenu(menu);
	      MenuInflater inflater = getMenuInflater();
	      inflater.inflate(R.menu.menu, menu);
	      return true;
	}
	
	// to handle selection of about or exit
	@Override
	   public boolean onOptionsItemSelected(MenuItem item) {
	      switch (item.getItemId()) {
	      case R.id.about:
	         startActivity(new Intent(this, About.class));
	         return true;
	      case R.id.exit:
	    	  finish();
	          break;
	      }
	      return false;
	   }

	// handles computation for item added, or clears everything if clear button clicked
	public void onClick(View v) 
	{
		if(costItems.length() == 0)
		{
			costItems.setText("0");
		}
		else if(numberPeopleOrItems.length() == 0)
		{
			numberPeopleOrItems.setText("0");
		}
		switch(v.getId())
		{
		case R.id.addButton:
			if(restaurantExpense)
			{
				total = total + (Double.parseDouble(costItems.getText().toString()));
				total = total + (total * (salesTax / 100));
				costItems.setText("");
				initialTotal.setText(df.format(total));
			}
			else
			{
				total = total + ((Double.parseDouble(costItems.getText().toString()) * Integer.parseInt(numberPeopleOrItems.getText().toString())));
				total = total + (total * (salesTax / 100));
				costItems.setText("");
				initialTotal.setText("");
				numberPeopleOrItems.setText("1");
				finalTotal = total;
				grandTotal.setText("$" + (df.format(finalTotal)));
			}
			break;
		case R.id.resetButton:
			numberPeopleOrItems.setText("1");
			costItems.setText("");
			initialTotal.setText("");
			grandTotal.setText("$0");
			if(restaurantExpense)
			{
				tip.setText("$0");
				evenSplit.setText("$0");
			}
			else
			{
				tip.setText("");
				evenSplit.setText("");
			}
			total = 0.0;
			finalTotal = 0.0;
			tipValue = 0.0;
			otherEntry.setText("10");
			tipValues.check(0);
		}
		
	}
	
	// handles display of drop down menu of states
	public void onItemSelected(AdapterView<?> parent, View v, int position, long id) 
	{	
		salesTax = stateTaxes.get(states[position]);
		selection.setText(taxValues[position].toString());
	}
	
	public void onNothingSelected(AdapterView<?> parent) 
	{
		selection.setText("");
		salesTax = stateTaxes.get(states[0]);
	}
	
	// to clear all entries if initial radio button is changed, and switch display
	public void onCheckedChanged(RadioGroup group, int checkedId) 
	{
		if(group == mode)
		{
			if(checkedId == R.id.restaurant_mode)
			{
				if(restaurantExpense == false)
				{
					total = 0.00;
					finalTotal = 0.00;
					numberPeopleOrItems.setText("1");
					otherEntry.setText("10");
				}
				numPeopleName.setText("No. of People");
				totalBeforeTipString.setText("Total Before Tip: ");
				gratuityString.setText("Gratuity Percent");
				tipAmountString.setText("Tip Amount: ");
				evenSplitString.setText("Even Split: ");
				restaurantExpense = true;
				tipValues.setVisibility(0);
				otherEntry.setVisibility(0);
				otherEntry.setEnabled(true);
				tipValues.setEnabled(true);
			}
			else
			{
				numPeopleName.setText("No. of Items");
				total = 0.00;
				finalTotal = 0.00;
				numberPeopleOrItems.setText("1");
				tip.setText("");
				totalBeforeTipString.setText("");
				tipAmountString.setText("");
				gratuityString.setText("");
				initialTotal.setText("");
				evenSplitString.setText("");
				evenSplit.setText("");
				restaurantExpense = false;
				tipValues.setVisibility(-1);
				otherEntry.setVisibility(-1);
				otherEntry.setEnabled(false);
				tipValues.setEnabled(false);
			}
		}
		
		// final computations of total, tip, and even split
		if(restaurantExpense)
		{
			if(checkedId==R.id.noTip)
			{
				tip.setText("$0.00");
				finalTotal = total;
				grandTotal.setText("$" + (df.format(finalTotal)));
				evenSplit.setText("$" + df.format((finalTotal / Integer.parseInt(numberPeopleOrItems.getText().toString()))));
			}
			else if(checkedId==R.id.fifteenPercent)
			{
				tipValue = total * 0.15;
				tip.setText("$" + df.format(tipValue));
				finalTotal = tipValue + total;
				grandTotal.setText("$" + (df.format(finalTotal)));
				evenSplit.setText("$" + df.format((finalTotal / Integer.parseInt(numberPeopleOrItems.getText().toString()))));
			}
			else if(checkedId==R.id.twentyPercent)
			{
				tipValue = total * 0.20;
				tip.setText("$" + df.format(tipValue));
				finalTotal = tipValue + total;
				grandTotal.setText("$" + (df.format(finalTotal)));
				evenSplit.setText("$" + df.format((finalTotal / Integer.parseInt(numberPeopleOrItems.getText().toString()))));
			}
			else
			{
				if(otherEntry.length() == 0)
				{
					otherEntry.setText("10");
				}
				tipValue = total * (Integer.parseInt(otherEntry.getText().toString())) / 100;
				tip.setText("$" + df.format(tipValue));
				finalTotal = tipValue + total;
				grandTotal.setText("$" + (df.format(finalTotal)));
				evenSplit.setText("$" + df.format((finalTotal / Integer.parseInt(numberPeopleOrItems.getText().toString()))));
			}
		}
		else
		{
			finalTotal = total;
			grandTotal.setText("$" + (df.format(finalTotal)));
		}
		
	}
}