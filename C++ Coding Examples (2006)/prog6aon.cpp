// prog6aon.cpp
// Aaron Ngbor
// Computer Science 1428
// Section 2
// Program 6
// Test Averages
// Input: This program reads all of the values from an input file. Each line
   // contains each student's ID number, major code, and test score.
// Output: The program outputs the total number of computer scienc majors, the
   // number of other majors, and the average grade for each of the two major
   // classifications to an output file.


#include<iostream>
#include<fstream>
#include<iomanip>
using namespace std;

int main()
{
    ifstream fin;                       // The variable for file input
    ofstream fout;                      // The variable for file output
    
    const int STUDENTS = 34;            // Number of students in the class
    int studID;                         // The student ID number
    int majorCode;                      // The student major code
    int testScore;                      // The student test score
    int numCompSci;                     // Number of computer science majors
    int numNonComp;                     // Number of other majors
    float average1 = 0;                 // Computer science average
    float average2 = 0;                 // Other major average
    
    fin.open("prog6.inp");
    fout.open("prog6out.txt");
    
    int i;                              // loop index variable
    int count = 0;                      // count variable for Computer Science
    int count2 = 0;                     // count variable for Non Comptuer Science
    for(i=1;i<=STUDENTS;i++)
    {
         fin >> studID;
         fin >> majorCode;
         fin >> testScore;
         
         if(majorCode == 422)
         {
             count++;
             numCompSci = count;
             average1 = (average1 + testScore);
                                  
         }
         
         else
         {
             count2++;
             numNonComp = count2; 
             average2 = (average2 + testScore);  
         }
     }
     
     average1 = average1 / numCompSci;
     average2 = average2 / numNonComp;
     
     fout << "Professor Ngbor's Test Statistics\n\n";
     fout << "Number of Computer Science Majors: ";
     fout << numCompSci << "\n";
     fout << "Number of Other Majors: ";
     fout << numNonComp << "\n";
     fout << "Class average of Computer Science Majors: ";
     fout << setprecision(1) << fixed << showpoint;
     fout << average1 << "\n";
     fout << "Class average for Other Majors: ";
     fout << average2 << "\n";
     
     fin.close();
     return 0;
     }
    
