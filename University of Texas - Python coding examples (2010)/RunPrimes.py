# -------
# imports
# -------

import sys

from Primes import primes_read, primes_eval, primes_print

# ----
# main
# ----

a = [0] * 4
b = [0]

while primes_read(sys.stdin, b) :
    c = primes_eval(b[0], a)
    primes_print(sys.stdout, a, c)
