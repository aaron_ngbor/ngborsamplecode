package com.hakoware.inmotionphotoexplorer;

/**
 * Created by aaron on 2/12/16.
 * GridItem class to create GridItem objects, containing a title string and URL string for the image.
 */
public class GridItem {
    private String image;
    private String title;

    public GridItem() {
        super();
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}