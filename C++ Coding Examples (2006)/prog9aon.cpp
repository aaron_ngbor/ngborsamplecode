// prog9aon.cpp
// Aaron Ngbor
// Computer Science 1428
// Section 2
// Program 9
// Diving scores
// Input: This program reads the needed data from an input file into an array.
   // Each line contains a diver name, their 8 scores from the judges, and a
   // number that represents the difficulty level of the dive.
// Output: The program will output each diver's name, final score, and the
   // individual score from each judge to an output file.

#include<iostream>
#include<iomanip>
#include<fstream>
using namespace std;


int main()
{
    
    
    ifstream fin;               // Variable for file input
    ofstream fout;              // Variable for file output
    
    const int JUDGE = 8;        // Number of scores given to each diver
    const int DIVERS = 10;      // Number of divers
    float scores[JUDGE];        // Array to hold the scores of each diver
    string diver;               // Name of diver
    float score;                // The score of each dive
    float highScore;            // The high score           
    float lowScore;             // The low score
    float diff;                 // The level of difficulty of the dive
    float sum;                  // The sum of all scores for each diver
    float finalScore;           // The final score for each diver
    finalScore = 0;
    sum = 0;
    
    fin.open("prog9inp.txt");
    fout.open("prog9out.txt");
    
    fout << "The Olympic Diving Finalists Results\n\n";
    for(int i=1; i <=DIVERS; i++)
    {
        fin >> diver;
        highScore=0;
        lowScore=100;
        sum=0;
        for(int i=0; i<8; i++)
        {
            fin >> scores[i];
            if(scores[i] > highScore)
           {
               highScore = scores[i];
           }
           else if(scores[i] < lowScore)
           {
               lowScore = scores[i];
           }
           sum = sum+scores[i];
        }
        fin >> diff;
        sum = sum - (lowScore + highScore);
        finalScore = sum * diff;
        fout << "Diver name: " << diver << "\n";
        fout << setprecision(2) << fixed << showpoint;
        fout << "The final score: " << finalScore << "\n";
        fout << "Individual judges scores:" << "\n"; 
        fout << setprecision(1) << fixed << showpoint;
        for(int i=0; i<JUDGE; i++)
        {
            fout << scores[i] << "\n";
        }
        fout << "\n";
    }
    fin.close();
    fout.close();  
    return 0;
}

        
